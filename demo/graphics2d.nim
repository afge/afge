import afge/display/window
import afge/graphics/[glw, image]
import afge/renderer/twod/renderer2d
import afge/utils/timing

proc main() =
  initWindowSystem()
  let window = createWindow("pog", 1280, 720, WindowInitData(samples: 4, resizable: true))
  window.makeContextCurrent()
  loadGLFunctions()
  enableDebug()

  var renderer = initRenderer2D()
  let texture = createTextureFromImage(loadImage("demo/res/kuroe.jpg", uint8, ColorComponent.RGBA), true)

  var loop = initFixedTimeLoop(60, 60, window)
  loop.update = proc (delta: Duration) = discard
  loop.render = proc () =
    setClearColor(COLOR_WHITE)
    clearFramebuffer({ClearTarget.COLOR_BUFFER, ClearTarget.DEPTH_BUFFER})

    let (w, h) = window.getFramebufferSize()
    renderer.beginFrame(float32 w, float32 h)

    renderer.addShape(texturePaint(texture),
      Vertex(pos: vec2f(100, 200), tcoords: vec2f(0, 0), color: vec4f(1.0, 0.0, 0.0, 1.0)),
      Vertex(pos: vec2f(700, 400), tcoords: vec2f(1, 0), color: vec4f(1.0, 0.0, 1.0, 1.0)),
      Vertex(pos: vec2f(300, 600), tcoords: vec2f(0, 1), color: vec4f(1.0, 1.0, 0.0, 1.0))
    )

    renderer.rect(
      500.0f32, 20.0f32, 160.0f32, 90.0f32,
      [COLOR_RED, COLOR_YELLOW, COLOR_BLUE, COLOR_LIME]
    )

    renderer.endFrame()
  loop.run()

  renderer.destroy()
  texture.destroy()

  window.destroy()
  terminateWindowSystem()

when isMainModule:
  main()