import ../wrapper/stb
import glw
import color
import ../utils/span

type
  ImageComponentType = uint8 | float32

  Image*[D: static[range[1..3]], T: ImageComponentType] = object
    dimensions*: array[D, int]
    data*: ptr T  # owned
    components*: ColorComponent

  Image1D*[T] = Image[1, T]
  Image2D*[T] = Image[2, T]
  Image3D*[T] = Image[3, T]

proc width*(s: Image): int =
  return s.dimensions[0]

proc height*(s: Image): int =
  return s.dimensions[1]

proc depth*(s: Image): int =
  return s.dimensions[2]

template destroy*(image: Image) =
  stbiImageFree cast[pointer](image.data)

proc loadImage[T](path: string, t: typedesc[T], desiredChannels: cint, flipY: bool): Image2D[T] =
  stbiSetFlipVerticallyOnLoad(cint flipY)
  var width, height, comps: cint
  result = Image2D[T](components: ColorComponent.RGBA)
  when T is uint8:
    result.data = cast[ptr uint8](stbiLoad(path, addr width, addr height, addr comps, cint desiredChannels))
  elif T is float32:
    result.data = stbiLoadf(path, addr width, addr height, addr comps, cint desiredChannels)
  if result.data == nil:
    return
  result.dimensions[0] = int width
  result.dimensions[1] = int height
  if desiredChannels == 0:
    result.components = cast[ColorComponent](comps)
  else:
    result.components = cast[ColorComponent](desiredChannels)

proc loadImage*[T](path: string, t: typedesc[T], flipY = true): Image2D[T] =
  return loadImage[T](path, t, STBI_default, flipY)

proc loadImage*[T](path: string, t: typedesc[T], desiredChannels: ColorComponent, flipY = true): Image2D[T] =
  return loadImage[T](path, t, cast[cint](desiredChannels), flipY)

proc loadImage*(path: string, desiredChannels = STBI_default, flipY = true): Image2D[uint8] =
  return loadImage(path, uint8, desiredChannels, flipY)

proc toDefaultTextureInternalFormat(component: ColorComponent): TextureInternalFormat =
  result = case component:
    of ColorComponent.RED: TextureInternalFormat.R8
    of ColorComponent.RED_GREEN: TextureInternalFormat.RG8
    of ColorComponent.RGB, ColorComponent.GRAY: TextureInternalFormat.RGB8
    else: TextureInternalFormat.RGBA8

proc toDefaultTextureFormat(component: ColorComponent): TextureFormat =
  result = case component:
    of ColorComponent.RED: TextureFormat.R
    of ColorComponent.RED_GREEN: TextureFormat.RG
    of ColorComponent.RGB, ColorComponent.GRAY: TextureFormat.RGB
    else: TextureFormat.RGBA

proc malloc(size: csize_t): pointer {.header: "<stdlib.h>", importc: "malloc".}
proc malloc[T](size: int): auto =
  when T is void:
    return malloc(csize_t size)
  else:
    return cast[ptr T](malloc(csize_t size * sizeof T))

proc convertGrayscaleImageAlpha(image: Image): Image =
  result = Image(components: ColorComponent.RGBA)
  var pixelCount = 1
  for i in 0 ..< result.dimensions.len:
    result.dimensions[i] = image.dimensions[i]
    pixelCount *= result.dimensions[i]
  result.data = malloc[Image.T](pixelCount * sizeof Color[ColorComponent.RGBA, Image.T])
  let dst = newSpan(cast[ptr Color[ColorComponent.RGBA, Image.T]](result.data), pixelCount)
  let src = newSpan(cast[ptr Color[ColorComponent.GRAY_ALPHA, Image.T]](image.data), pixelCount)
  for i in 0 ..< pixelCount:
    dst[i] = convertColor[ColorComponent.RGBA, Image.T, ColorComponent.GRAY_ALPHA, Image.T] src[i]

proc convertGrayscaleImageNoAlpha(image: Image): Image =
  result = Image(components: ColorComponent.RGB)
  var pixelCount = 1
  for i in 0 ..< result.dimensions.len:
    result.dimensions[i] = image.dimensions[i]
    pixelCount *= result.dimensions[i]
  result.data = malloc[Image.T](pixelCount * sizeof Color[ColorComponent.RGB, Image.T])
  let dst = newSpan(cast[ptr Color[ColorComponent.RGB, Image.T]](result.data), pixelCount)
  let src = newSpan(cast[ptr Color[ColorComponent.GRAY, Image.T]](image.data), pixelCount)
  for i in 0 ..< pixelCount:
    dst[i] = convertColor[ColorComponent.RGB, Image.T, ColorComponent.GRAY, Image.T] src[i]

proc convertGrayscaleImage(image: Image): Image =
  case image.components:
  of ColorComponent.GRAY:
    return convertGrayscaleImageNoAlpha(image)
  of ColorComponent.GRAY_ALPHA:
    return convertGrayscaleImageAlpha(image)
  else:
    discard

proc createTextureFromImage*(image: Image1D, free = false): Texture1D =
  if image.data == nil:
    return
  case image.components:
  of ColorComponent.GRAY, ColorComponent.GRAY_ALPHA:
    result = createTextureFromImage(convertGrayscaleImage(image), true)
  else:
    result = createGL(Texture1D)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
    result.allocTextureData(1, image.components.toDefaultTextureInternalFormat(), image.width)
    var typ: TextureDataType
    when Image1D.T is uint8:
      typ = TextureDataType.UBYTE
    else:
      typ = TextureDataType.FLOAT
    
    result.setTextureData(0, 0, image.width, image.components.toDefaultTextureFormat(), typ, cast[pointer](image.data))
  if free:
    destroy image

proc createTextureFromImage*(image: Image2D, free = false): Texture2D =
  if image.data == nil:
    return
  case image.components:
  of ColorComponent.GRAY, ColorComponent.GRAY_ALPHA:
    result = createTextureFromImage(convertGrayscaleImage(image), true)
  else:
    result = createGL(Texture2D)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
    result.allocTextureData(1, image.components.toDefaultTextureInternalFormat(), image.width, image.height)
    var typ: TextureDataType
    when Image2D.T is uint8:
      typ = TextureDataType.UBYTE
    else:
      typ = TextureDataType.FLOAT
    
    result.setTextureData(0, 0, 0, image.width, image.height, image.components.toDefaultTextureFormat(), typ, cast[pointer](image.data))
  if free:
    destroy image

proc createTextureFromImage*(image: Image3D, free = false): Texture3D =
  if image.data == nil:
    return
  case image.components:
  of ColorComponent.GRAY, ColorComponent.GRAY_ALPHA:
    result = createTextureFromImage(convertGrayscaleImage(image), true)
  else:
    result = createGL(Texture3D)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
    result.allocTextureData(1, image.components.toDefaultTextureInternalFormat(), image.width, image.height, image.depth)
    var typ: TextureDataType
    when Image3D.T is uint8:
      typ = TextureDataType.UBYTE
    else:
      typ = TextureDataType.FLOAT
    
    result.setTextureData(0, 0, 0, 0, image.width, image.height, image.depth, image.components.toDefaultTextureFormat(), typ, cast[pointer](image.data))
  if free:
    destroy image
