import glw
import glm
import ../utils/own
import ../layout/layoututils

import sets

export glm

type
  AttribDataType* = int8|int16|int32 | uint8|uint16|uint32 | float32|float64

  ElementBuffer* = object
    buffer*: Own[Buffer]
    count*: int
    elementType*: ElementType

  Mesh* = object
    vertexArray*: Own[VertexArray]
    ownedAttribBuffers*: HashSet[GLuint]
    elementBuffer*: ElementBuffer
    currentAttrib*: int
    # only useful in DSA contexts
    currentBinding*: int

proc createMesh*(vertexArray: Own[VertexArray]): Mesh =
  result.vertexArray = vertexArray
  result.ownedAttribBuffers.init()

proc createMesh*(vertexArray: VertexArray): Mesh =
  return createMesh notOwn(vertexArray)

proc createMesh*(): Mesh =
  return createMesh own(createGL(VertexArray))

proc destroy*(mesh: Mesh) =
  mesh.vertexArray.ifOwnVoid(proc(va: VertexArray) = destroy va)
  mesh.elementBuffer.buffer.ifOwnVoid(proc(b: Buffer) = destroy b)
  for buffer in mesh.ownedAttribBuffers:
    destroy Buffer buffer

proc bindGL*(mesh: Mesh) =
  mesh.vertexArray.value.bindVertexArray()

{.push inline.}
proc getVectorSize[T: SomeInteger | SomeFloat](t: typedesc[T]): int =
  return 1

proc getVectorSize[N, E; T: Vec[N, E]](t: typedesc[T]): int =
  return N

proc getVectorSize[M, N, E; T: Mat[M, N, E]](t: typedesc[T]): int =
  return M * N

proc getVectorElementType[T: int8|int16|int32 | uint8|uint16|uint32 | float32|float64](t: typedesc[T]): AttribType =
  when T is int8:
    return AttribType.ATTRIB_BYTE
  elif T is int16:
    return AttribType.ATTRIB_SHORT
  elif T is int32:
    return AttribType.ATTRIB_INT
  elif T is uint8:
    return AttribType.ATTRIB_UBYTE
  elif T is uint16:
    return AttribType.ATTRIB_USHORT
  elif T is uint32:
    return AttribType.ATTRIB_UINT
  elif T is float32:
    return AttribType.ATTRIB_FLOAT
  elif T is float64:
    return AttribType.ATTRIB_DOUBLE

proc getVectorElementType[N, E; T: Vec[N, E]](t: typedesc[T]): AttribType =
  return getVectorElementType(E)

proc getVectorElementType[M, N, E; T: Mat[M, N, E]](t: typedesc[T]): AttribType =
  return getVectorElementType(E)

static:
  assert getVectorElementType(float32) == ATTRIB_FLOAT
  assert getVectorElementType(Vec3f) == ATTRIB_FLOAT
  assert getVectorElementType(Mat3x2i) == ATTRIB_INT
  assert getVectorSize(float32) == 1
  assert getVectorSize(Vec3f) == 3
  assert getVectorSize(Mat2f) == 4
{.pop.}

proc addAttribBuffer*[B: Buffer](mesh: var Mesh, buffer: B) =
  mesh.ownedAttribBuffers.incl GLuint(buffer)

proc bindTypedBuffer*(mesh: var Mesh, buffer: TypedBuffer) =
  if dsaEnabled():
    var tmp: TypedBuffer.T
    mesh.vertexArray.value.bindVertexBufferDSA(Buffer buffer, mesh.currentBinding, luSizeof tmp, 0)
    template callback(fieldName: static[string], fieldValue: typed) =
      let size = getVectorSize(typeof(fieldValue))
      let elementType = getVectorElementType(typeof(fieldValue))
      let offset = cast[int](unsafeAddr fieldValue) - cast[int](unsafeAddr tmp)

      mesh.vertexArray.value.setVertexAttributeDSA(mesh.currentAttrib, mesh.currentBinding, cast[AttribSize](size), elementType, offset)
      mesh.vertexArray.value.enableVertexAttribs(mesh.currentAttrib)
      inc(mesh.currentAttrib)
    tmp.luFieldPairs(callback)
    inc(mesh.currentBinding)
  else:
    mesh.bindGL()
    glBindBuffer(GL_ARRAY_BUFFER, GLuint buffer)
    var tmp: TypedBuffer.T
    for fieldName, fieldValue in tmp.fieldPairs:
      let size = getVectorSize(typeof(fieldValue))
      let elementType = getVectorElementType(typeof(fieldValue))
      let offset = cast[int](unsafeAddr fieldValue) - cast[int](unsafeAddr tmp)

      mesh.vertexArray.value.setVertexAttribute(buffer, mesh.currentAttrib, cast[AttribSize](size), elementType, sizeof tmp, offset)
      mesh.vertexArray.value.enableVertexAttribs(mesh.currentAttrib)
      inc(mesh.currentAttrib)

proc addTypedBuffer*[T](mesh: var Mesh, t: typedesc[T]): TypedBuffer[T] {.discardable.} =
  let buffer: TypedBuffer[T] = createGL(Buffer)
  mesh.addAttribBuffer(buffer)
  mesh.bindTypedBuffer(buffer)
  return buffer

proc setElementBuffer*(mesh: var Mesh, buffer: Own[Buffer]) =
  mesh.elementBuffer.buffer = buffer

  if dsaEnabled():
    mesh.vertexArray.value.bindElementBufferDSA(mesh.elementBuffer.buffer.value)

proc createElementBuffer*[T: uint8|uint16|uint32](mesh: var Mesh, data: openArray[T], usage = DYNAMIC_DRAW) =
  let buffer: TypedBuffer[T] = createGL(Buffer)
  buffer.setBufferData(data, usage)  
  mesh.setElementBuffer own(buffer)
  mesh.elementBuffer.count = data.len

  when T is uint8:
    mesh.elementBuffer.elementType = ElementType.UBYTE
  elif T is uint16:
    mesh.elementBuffer.elementType = ElementType.USHORT
  elif T is uint32:
    mesh.elementBuffer.elementType = ElementType.UINT
    
proc draw*(mesh: Mesh, drawType: DrawType) =
  mesh.bindGL()
  if GLuint(mesh.elementBuffer.buffer.value) == 0:
    drawArrays(drawType, 0, mesh.elementBuffer.count)
  else:
    if not dsaEnabled():
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GLuint mesh.elementBuffer.buffer.value)
    drawElements(drawType, mesh.elementBuffer.count, mesh.elementBuffer.elementType, 0)
  
