import math, strformat
import afge/display/window
import afge/graphics/[glw, mesh, image, shader_utils]
import afge/utils/[timing, debugcam]
import afge/layout/[glsl, layoututils]
import afge/model/[scene, animation]
import afge/shader/glsli

const BONE_MATRICES_COUNT = 253
type
  MatrixSuite {.std140.} = object
    proj: Mat4f
    view: Mat4f
    trans: Mat4f
    boneMatrices: array[BONE_MATRICES_COUNT, Mat4f]

proc main() =
  initWindowSystem()
  let wnd = createWindow("pog", 1280, 720, WindowInitData(samples: 4, resizable: true))
  wnd.makeContextCurrent()
  loadGLFunctions()

  enableDepthTest()
  enableCulling(CullFace.BACK)

  let model = loadScene("demo/res/model.dae", slobPresetRealTimeQuality)
  var mesh = createMesh()
  var bones = model.meshes[0].bakeMesh([
    SceneMeshAttributeType.POSITION.dimensions(3),
    SceneMeshAttributeType.TEXCOORDS.index(0),
    SceneMeshAttributeType.BONE_DATA], mesh)
  let animation = model.animations[0].bakeAnimation(model, bones)
  var animator = animation.newAnimator()
  model.destroy()
  let diffuse = loadImage("demo/res/diffuse.png").createTextureFromImage(free = true)
  const 
    curdir = getProjectDir()
    defines = fmt"""
BONE_MATRICES_COUNT {BONE_MATRICES_COUNT}
NOGLSLLINT"""
    includes = fmt"""
{curdir}
"""

  let program = createShaderProgram(
    (ShaderType.Vertex, glsli("shaders/model.vert", defines = defines, includes = includes)),
    (ShaderType.Fragment, glsli("shaders/model.frag", defines = defines, includes = includes))
  ).getProgram()

  let matrixSuiteBuffer = createGL(TypedBuffer[MatrixSuite])
  matrixSuiteBuffer.allocDataImmutableTyped(1, {ImmutableBufferUsage.MAP_WRITE})
  matrixSuiteBuffer.bindUniformBuffer(1)

  var camera = defaultDebugCamera()
  var loop = initFixedTimeLoop(60, 60, wnd)
  var boneTransform: array[BONE_MATRICES_COUNT, Mat4f]
  let boneTransformSpan = newSpan(addr boneTransform[0], boneTransform.len)
  
  loop.update = proc (deltaDuration: Duration) =
    let delta = deltaDuration.inFloatSeconds()
    camera.handleInput(wnd, delta)
    animator.update(delta, boneTransformSpan)

  loop.render = proc () =
    setClearColor(COLOR_WHITE)
    clearFramebuffer({ClearTarget.COLOR_BUFFER, ClearTarget.DEPTH_BUFFER})
    let (fbw, fbh) = wnd.getFramebufferSize()

    var matrixSuite = matrixSuiteBuffer.mapTyped(0, 1, {BufferMapAccess.WRITE})
    matrixSuite.proj = perspective(radians(90.0f32), float32(fbw / fbh), 0.1f32, 1000.0f32)
    matrixSuite.view = camera.toMatrix()
    matrixSuite.trans = mat4f(1.0)
    matrixSuite.boneMatrices = boneTransform
    if not matrixSuiteBuffer.unmap():
      loop.stop()

    program.use()
    diffuse.bindTex(0)
    mesh.draw(Triangle)

  loop.run()

  mesh.destroy()
  program.destroy()
  diffuse.destroy()
  matrixSuiteBuffer.destroy()

  terminateWindowSystem()

when isMainModule:
  main()