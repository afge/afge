// currently not working
constexpr int BONE_MATRICES_COUNT = 253;

namespace vertex {
    [[spirv::in(0)]] vec3 pos;
    [[spirv::in(1)]] vec2 tcoords;
    [[spirv::in(2)]] vec4 boneWeights;
    [[spirv::in(3)]] ivec4 boneIds;

    [[spirv::out(0)]] vec2 outTCoords;
}

namespace fragment {
    [[spirv::in(0)]] vec2 tcoords;

    [[spirv::out(0)]] vec4 outColor;
}

struct MatrixSuite {
    mat4 proj, view, trans;
    mat4 boneTransforms[BONE_MATRICES_COUNT];
};

[[spirv::uniform(0)]]
MatrixSuite matrixSuite;

[[spirv::uniform(0)]]
sampler2D diffuse;

[[spirv::vert]]
void vertex_main() {
    using namespace vertex;
    outTCoords = vertex::tcoords;
    vec4 totalPos = vec4(0.0);
    for(int i = 0; i < sizeof(vec4)/sizeof(float); i++) {
        if(vertex::boneIds[i] < 0)  continue;
        if(vertex::boneIds[i] >= BONE_MATRICES_COUNT) {
            totalPos = vec4(vertex::pos, 1.0);
            break;
        }

        vec4 localPos = matrixSuite.boneTransforms[vertex::boneIds[i]] * vec4(vertex::pos, 1.0);
        totalPos += localPos * vertex::boneWeights[i];
    }
    glvert_Output.Position = matrixSuite.proj * matrixSuite.view * matrixSuite.trans * totalPos;
}

[[spirv::frag]]
void fragment_main() {
    fragment::outColor = texture(diffuse, fragment::tcoords);
}