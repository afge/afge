import math
import afge/display/window
import afge/graphics/[glw, mesh, image, shader_utils]
import afge/utils/[timing, debugcam]
import afge/layout/[dynamic_struct, glsl, layoututils]

type
  Vertex = object
    pos: Vec2f
    tcoords: Vec2f

  Transform = object
    pos: Vec3f
    rotation: Quatf
    scale: Vec3f

  MatrixSuite {.std140.} = object
    proj: Mat4f
    view: Mat4f
    trans: Mat4f

  BlendingOptions {.std140.} = object
    alpha: float32  

dynamic:  # alignment depends on hardware
  type
    UniformBufferData* = object
      suite: MatrixSuite
      blending: BlendingOptions

proc trans2mat(transform: Transform): Mat4f =
  result = poseMatrix(transform.pos, transform.rotation, transform.scale)

proc main() =
  initWindowSystem()

  let window = createWindow("thank you based shaft/doroinu/etc.", 1280, 720, WindowInitData(samples: 4, resizable: false))
  window.makeContextCurrent()
  loadGLFunctions()

  enableDebug()
  enableAlphaBlending()

  var mesh = createMesh()

  let vboData = [
    Vertex(pos: vec2(-1.0f, 1.0f), tcoords: vec2(0.0f, 1.0f)),
    Vertex(pos: vec2(1.0f, 1.0f), tcoords: vec2(1.0f, 1.0f)),
    Vertex(pos: vec2(1.0f, -1.0f), tcoords: vec2(1.0f, 0.0f)),
    Vertex(pos: vec2(-1.0f, -1.0f), tcoords: vec2(0.0f, 0.0f))
  ]

  let eboData = [0u32, 1u32, 2u32, 2u32, 3u32, 0u32]

  mesh.addTypedBuffer(Vertex).allocDataImmutableTyped(vboData)
  mesh.createElementBuffer(eboData, StaticDraw)

  let texture = createTextureFromImage(loadImage("demo/res/kuroe.jpg", uint8, ColorComponent.RGBA), true)

  let program = createShaderProgram(
    (ShaderType.Vertex, glsl"""#version 430
layout (location = 0) in vec2 pos;
layout (location = 1) in vec2 tcoords;

layout (location = 0) out vec2 vf_tcoords;

layout (std140, binding = 1) uniform MatrixSuite {
  mat4 proj;
  mat4 view;
  mat4 trans;
};

void main() {
  gl_Position = proj * view * trans * vec4(pos, 0.0, 1.0);
  vf_tcoords = tcoords;
}    
    """),
    (ShaderType.Fragment, glsl"""#version 430
layout (location = 0) in vec2 vf_tcoords;
layout (location = 0) out vec4 color;

layout (binding = 0) uniform sampler2D tex;

layout (std140, binding = 2) uniform BlendingOptions {
  float alpha;
};

void main() {
  color = texture(tex, vf_tcoords);
  color.a = alpha;
}
    """)
  ).getProgram()

  UniformBufferData.calculateUniformLayout()
  var uniformBuffer = createGL(TypedBuffer[UniformBufferData])
  uniformBuffer.allocDataImmutableTyped(1, {ImmutableBufferUsage.MAP_WRITE})
  uniformBuffer.bindUniformBufferTyped(1, suite)
  uniformBuffer.bindUniformBufferTyped(2, blending)

  var transform = Transform(pos: vec3(0.0f32, 0.0f32, -1.0f32), rotation: quatf(), scale: vec3(1.6f32, 0.9f32, 1.0f32))
  var camera = defaultDebugCamera()

  var loop = initFixedTimeLoop(60, 60, window)

  loop.update = proc (deltaDuration: Duration) =
    let delta = deltaDuration.inFloatSeconds()
    let time = getTime().toUnixFloat()
    transform.pos.x = float32 sin(time)
    transform.pos.z = float32 cos(time)
    camera.handleInput(window, delta)

  loop.render = proc () =
    setClearColor(COLOR_WHITE)
    clearFramebuffer({ClearTarget.COLOR_BUFFER})
    let (fbw, fbh) = window.getFramebufferSize()

    var ubData = uniformBuffer.mapTyped(0, 1, {BufferMapAccess.WRITE})
    ubData.blending.alpha = glfwGetTime().splitDecimal().floatpart
    ubData.suite.proj = perspective(radians(90.0f32), float32(fbw / fbh), 0.1f32, 1000.0f32)
    ubData.suite.trans = trans2mat(transform)
    ubData.suite.view = camera.toMatrix()
    if not uniformBuffer.unmap():
      loop.stop()

    program.use()
    texture.bindTex(0)
    mesh.draw(Triangle)

  loop.run()

  uniformBuffer.destroy()
  mesh.destroy()
  texture.destroy()
  program.destroy()

  window.destroy()
  terminateWindowSystem()

when isMainModule:
  main()