import macros
import glw
import glm
import strformat
import strutils

import ../layout/layoututils

# export strformat

const afgeBraceOpen* = '{'
const afgeBraceClose* = '}'

template getUniformLocation*(program: ShaderProgram, name: string): int =
  glGetUniformLocation(GLuint program, cstring name)

macro genSetUniform(cases: varargs[untyped]) =
  let program = ident("program")
  let name = ident("name")
  let value = ident("value")
  let location = ident("location")
  result = quote do:
    proc setUniform*[T](`program`: ShaderProgram, `name`: string, `value`: T): bool =
      when T is array or T is seq:
        result = true
        for i in 0 ..< value.len:
          result &= setUniform(`program`, `name` & "[" & $i & "]", `value`[i])
      elif T is tuple or T is object:
        result = true
        for fieldname, fieldvalue in value.fieldPairs():
          result &= setUniform(`program`, `name` & "." & fieldname, fieldvalue)
      else:
        let `location` = GLint getUniformLocation(`program`, `name`)
        if `location` < 0:
          return false

  var dst = nnkWhenStmt.newTree()

  for typeCase in cases:
    let typ = typeCase[0]
    let dsaEnabled = typeCase[1][0][0]
    let dsaDisabled = typeCase[1][0][1][1]
    assert $typeCase[1][0][1][0] == "otherwise"
    dst.add nnkElifBranch.newTree(
      nnkInfix.newTree(
        ident("is"),
        ident("T"),
        typ
      ),
      nnkStmtList.newTree(
        nnkIfStmt.newTree(
          nnkElifBranch.newTree(
            nnkCall.newTree(
              bindSym "dsaEnabled"
            ),
            nnkStmtList.newTree(
              dsaEnabled
            )
          ),
          nnkElse.newTree(
            nnkStmtList.newTree(
              dsaDisabled
            )
          )
        ),
        nnkReturnStmt.newTree(
          ident("true")
        )
      )
    )
  result[6][0][2][0].add dst
  dst.add nnkElse.newTree(
    quote do:
      return false
  )

genSetUniform:
of GLfloat: glProgramUniform1f(GLuint program, location, value) otherwise glUniform1f(location, value)
of GLdouble: glProgramUniform1d(GLuint program, location, value) otherwise glUniform1d(location, value)
of GLint: glProgramUniform1i(GLuint program, location, value) otherwise glUniform1i(location, value)
of GLuint: glProgramUniform1ui(GLuint program, location, value) otherwise glUniform1ui(location, value)
of GLint64: glProgramUniform1i64ARB(GLuint program, location, value) otherwise glUniform1i64ARB(location, value)
of GLuint64: glProgramUniform1ui64ARB(GLuint program, location, value) otherwise glUniform1ui64ARB(location, value)

of Vec2f: glProgramUniform2fv(GLuint program, location, unsafeAddr value) otherwise glUniform2fv(location, unsafeAddr value)
of Vec3f: glProgramUniform3fv(GLuint program, location, unsafeAddr value) otherwise glUniform3fv(location, unsafeAddr value)
of Vec4f: glProgramUniform4fv(GLuint program, location, unsafeAddr value) otherwise glUniform4fv(location, unsafeAddr value)
of Mat4x4f: glProgramUniformMatrix4fv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix4fv(location, unsafeAddr value)
of Mat4x3f: glProgramUniformMatrix4x3fv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix4x3fv(location, unsafeAddr value)
of Mat4x2f: glProgramUniformMatrix4x2fv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix4x2fv(location, unsafeAddr value)
of Mat3x4f: glProgramUniformMatrix3x4fv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix3x4fv(location, unsafeAddr value)
of Mat3x3f: glProgramUniformMatrix3fv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix3fv(location, unsafeAddr value)
of Mat3x2f: glProgramUniformMatrix3x2fv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix3x2fv(location, unsafeAddr value)
of Mat2x4f: glProgramUniformMatrix2x4fv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix2x4fv(location, unsafeAddr value)
of Mat2x3f: glProgramUniformMatrix2x3fv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix2x3fv(location, unsafeAddr value)
of Mat2x2f: glProgramUniformMatrix2fv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix2fv(location, unsafeAddr value)

of Vec2d: glProgramUniform2dv(GLuint program, location, unsafeAddr value) otherwise glUniform2dv(location, unsafeAddr value)
of Vec3d: glProgramUniform3dv(GLuint program, location, unsafeAddr value) otherwise glUniform3dv(location, unsafeAddr value)
of Vec4d: glProgramUniform4dv(GLuint program, location, unsafeAddr value) otherwise glUniform4dv(location, unsafeAddr value)
of Mat4x4d: glProgramUniformMatrix4dv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix4fv(location, unsafeAddr value)
of Mat4x3d: glProgramUniformMatrix4x3dv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix4x3dv(location, unsafeAddr value)
of Mat4x2d: glProgramUniformMatrix4x2dv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix4x2dv(location, unsafeAddr value)
of Mat3x4d: glProgramUniformMatrix3x4dv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix3x4dv(location, unsafeAddr value)
of Mat3x3d: glProgramUniformMatrix3dv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix3fv(location, unsafeAddr value)
of Mat3x2d: glProgramUniformMatrix3x2dv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix3x2dv(location, unsafeAddr value)
of Mat2x4d: glProgramUniformMatrix2x4dv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix2x4dv(location, unsafeAddr value)
of Mat2x3d: glProgramUniformMatrix2x3dv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix2x3dv(location, unsafeAddr value)
of Mat2x2d: glProgramUniformMatrix2dv(GLuint program, location, unsafeAddr value) otherwise glUniformMatrix2fv(location, unsafeAddr value)

of Vec2i: glProgramUniform2iv(GLuint program, location, unsafeAddr value) otherwise glUniform2iv(location, unsafeAddr value)
of Vec3i: glProgramUniform3iv(GLuint program, location, unsafeAddr value) otherwise glUniform3iv(location, unsafeAddr value)
of Vec4i: glProgramUniform4iv(GLuint program, location, unsafeAddr value) otherwise glUniform4iv(location, unsafeAddr value)
of Vec2ui: glProgramUniform2uiv(GLuint program, location, unsafeAddr value) otherwise glUniform2uiv(location, unsafeAddr value)
of Vec3ui: glProgramUniform3uiv(GLuint program, location, unsafeAddr value) otherwise glUniform3uiv(location, unsafeAddr value)
of Vec4ui: glProgramUniform4uiv(GLuint program, location, unsafeAddr value) otherwise glUniform4uiv(location, unsafeAddr value)

of Vec2l: glProgramUniform2i64vARB(GLuint program, location, unsafeAddr value) otherwise glUniform2i64vARB(location, unsafeAddr value)
of Vec3l: glProgramUniform3i64vARB(GLuint program, location, unsafeAddr value) otherwise glUniform3i64vARB(location, unsafeAddr value)
of Vec4l: glProgramUniform4i64vARB(GLuint program, location, unsafeAddr value) otherwise glUniform4i64vARB(location, unsafeAddr value)
of Vec2ul: glProgramUniform2ui64vARB(GLuint program, location, unsafeAddr value) otherwise glUniform2ui64vARB(location, unsafeAddr value)
of Vec3ul: glProgramUniform3ui64vARB(GLuint program, location, unsafeAddr value) otherwise glUniform3ui64vARB(location, unsafeAddr value)
of Vec4ul: glProgramUniform4ui64vARB(GLuint program, location, unsafeAddr value) otherwise glUniform4ui64vARB(location, unsafeAddr value)

proc bindUniformBuffer*(buffer: Buffer, binding: int, offset: int = 0, size: int = -1) =
  let realSize = if size < 0: buffer.getBufferSize() else: size
  glBindBufferRange(GL_UNIFORM_BUFFER, GLuint binding, GLuint buffer, GLintptr offset, GLsizeiptr realSize)

template bindUniformBufferTyped*[T](buffer: TypedBuffer[T], binding: int, field: untyped) =
  buffer.bindUniformBuffer(binding, luOffsetOf(T, field), luSizeofField(typedesc T, field))

# alternative to fmt/& for embedding glsl in nim code
# to add argument (like fmt"{hello}"), you add a dollar sign: glsl"${hello}"
macro glsl*(rawFormat: untyped): untyped = 
  type TokenType {.pure.} = enum
    FMT_BRACE_OPEN, FMT_BRACE_CLOSE
    BRACE_OPEN, BRACE_CLOSE,
    TEXT, NONE
  type Token = tuple[typ: TokenType, data: string]

  var currentToken = (typ: TokenType.NONE, data: "")
  var tokens = newSeq[Token]()
  var braceStack = newSeq[TokenType]()

  proc flushToken() =
    if currentToken.typ != TokenType.NONE:
      tokens.add currentToken
    currentToken.typ = TokenType.NONE
    currentToken.data = ""

  let str = $rawFormat
  var i = 0
  while i < str.len:
    if str[i] == '$' and i + 1 < str.len and str[i + 1] == '{':
      i += 2
      flushToken()
      tokens.add (typ: TokenType.FMT_BRACE_OPEN, data: "")
      braceStack.add TokenType.FMT_BRACE_OPEN
      continue
    elif str[i] == '{':
      i += 1
      flushToken()
      tokens.add (typ: TokenType.BRACE_OPEN, data: "")
      braceStack.add TokenType.BRACE_OPEN
    elif str[i] == '}':
      assert braceStack.len > 0
      let typ = case braceStack[^1]:
        of FMT_BRACE_OPEN: TokenType.FMT_BRACE_CLOSE
        of BRACE_OPEN: TokenType.BRACE_CLOSE
        else: TokenType.NONE
      assert typ != TokenType.NONE
      braceStack.del(braceStack.len - 1)
      i += 1
      flushToken()
      tokens.add (typ: typ, data: "")
    else:
      currentToken.typ = TokenType.TEXT
      currentToken.data &= str[i]
      i += 1
  flushToken()

  var format = ""
  for token in tokens:
    case token.typ:
    of BRACE_OPEN: format &= "{afgeBraceOpen}"
    of BRACE_CLOSE: format &= "{afgeBraceClose}"
    of FMT_BRACE_OPEN: format &= "{"
    of FMT_BRACE_CLOSE: format &= "}"
    of TEXT: format &= token.data
    else: discard

  let fmt = bindSym"fmt"
  return quote do:
    `fmt``format`

proc isVersionDirective(line: string): bool =
  if line.startsWith("#"):
    return line[1..^1].strip().startsWith("version")

proc glslWithMacros*(source: string, macros: openArray[string]): string =
  var addedMacros = false
  for line in source.splitLines():
    result.add line
    result.add "\n"

    if line.isVersionDirective() and not addedMacros:
      addedMacros = true
      for m in macros:
        result.add "#define "
        result.add m
        result.add "\n"

proc glslOverrideVersion*(source: string, newVersion: string): string =
  var hasVersionDirective = false
  for line in source.splitLines():
    if line.isVersionDirective():
      hasVersionDirective = true
      result.add "#version "
      result.add newVersion
    else:
      result.add line
    result.add "\n"

  if not hasVersionDirective:
    result = "#version " & newVersion & "\n" & result