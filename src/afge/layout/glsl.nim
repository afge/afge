import macros
import glm
import sugar

import ../utils/macroutils
import ../wrapper/gl
import layoututils
import dynamic_struct

# std140 and std430 structs
proc genGLSLStruct(typedecl: var NimNode, std140: bool) =
  proc transformField(identDef: var NimNode) =
    if identDef[0].kind == nnkIdent:
      identDef[0] = nnkPragmaExpr.newTree(
        identDef[0],
        nnkPragma.newTree()
      )

    identDef[0][1].add newCall(ident "align", newCall(ident("alignOfGlsl"), identDef[1], newLit std140))
  visitChildren(typedecl, {nnkRecList}, (n: var NimNode) => visitChildren(n, {nnkIdentDefs}, transformField))
  discard

proc roundup*(value: int, roundto: int): int =
  result = (value and not(roundto - 1)) + (if (value and (roundto - 1)) == 0: 0 else: roundto)
# only supports std140 and std430
proc alignOfGlsl*[T](t: typedesc[T], std140: bool): int =
  when T is Vec:
    var t: T
    if t.arr.len == 1:
      return alignOfGlsl(typeof t[0], std140)
    elif t.arr.len == 2:
      return alignOfGlsl(typeof t[0], std140) * 2
    else:
      return alignOfGlsl(typeof t[0], std140) * 4
  elif T is array:
    var t: T
    when t[0] is Mat:
      return alignOfGlsl(typeof t[0], std140)
    else:
      result = alignOfGlsl(typeof t[0], std140)
      if std140:
        result = roundup(result, 16)
  elif T is Mat:
    var t: T
    return alignOfGlsl(typeof t[0], std140)
  elif T is SomeNumber or T is bool:
    return if sizeof(T) < 4: 4 else: sizeof T
  elif T is object or T is tuple:
    result = alignof T
    if std140:
      result = roundup(result, 16)

macro std140*(arg: untyped): untyped =
  result = arg
  visitChildren(result, {nnkTypeDef}, (n: var NimNode) => genGLSLStruct(n, true))

proc calculateUniformLayout*[T](t: typedesc[T]) =
  var ubOffAlign: GLint
  glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, addr ubOffAlign)
  template callback(fieldName: static[string], fieldValue: typed) =
    if alignStr(t, fieldName) < ubOffAlign:
      alignStr(t, fieldName) = ubOffAlign
  var obj = (ptr T) nil
  luFieldPairs(obj, callback)
  t.calculateStructLayout()
