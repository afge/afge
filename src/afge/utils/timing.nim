import times, os, math
import ../display/window

export times

when defined(windows):
  import winlean

  type REASON_CONTEXT_Reason_Detailed {.pure.} = object
    LocalizedReasonModule: HANDLE
    LocalizedReasonId: ULONG
    ReasonStringCount: ULONG
    ReasonStrings: ptr WideCString
  type REASON_CONTEXT_Reason {.pure, union.} = object
    Detailed: REASON_CONTEXT_Reason_Detailed
    SimpleReasonString: WideCString
  type REASON_CONTEXT {.pure.} = object
    Version: ULONG
    Flags: DWORD
    Reason: REASON_CONTEXT_Reason
  type PTIMERAPCROUTINE = proc (lpArgToCompletionRoutine: pointer, dwTimerLowValue: DWORD, dwTimerHighValue: DWORD): void {.stdcall.}
  proc CreateWaitableTimer(lpTimerAttributes: ptr SECURITY_ATTRIBUTES, bManualReset: WINBOOL, lpTimerName: WideCString): HANDLE {.stdcall, dynlib: "kernel32", importc: "CreateWaitableTimerW".}
  proc SetWaitableTimerEx(hTimer: HANDLE, lpDueTime: ptr int64, lPeriod: LONG, pfnCompletionRoutine: PTIMERAPCROUTINE, lpArgToCompletionRoutine: pointer, WakeContext: ptr REASON_CONTEXT, TolerableDelay: ULONG): WINBOOL {.stdcall, dynlib: "kernel32", importc.}

type
  UpdateCallback* = proc (delta: Duration)
  RenderCallback* = proc ()
  InputHandleCallback* = proc ()
  SleepProc = proc (time: Duration)

  FixedTimeGameLoop* = object
    window: Window
    handleInput: InputHandleCallback
    updatecb: UpdateCallback
    rendercb: RenderCallback
    sleep: SleepProc
    stop: bool
    updateDelta: Duration
    renderDelta: Duration

  GameLoop* = FixedTimeGameLoop

proc inFloatSeconds*(duration: Duration): float = float(duration.inNanoseconds()) * 1e-9

# inaccurate
proc osSleep*(duration: Duration) =
  sleep(int duration.inMilliseconds())

# high CPU usage
proc spinLockSleep*(duration: Duration) =
  let start = getTime()
  while getTime() - start < duration:
    discard

# From: https://blat-blatnik.github.io/computerBear/making-accurate-sleep-function/
# works best on modern computers, but idk about older ones
# when useWindowsTimer is true, it will use win32 timers on windows machines, and fallback to the default behavior on unix/bsd etc.
# windows timer give a little less accuracy, but have less CPU usage compared to the fallback algorithm
# go to the website above for more info
proc accurateSleep[useWindowsTimer: static[bool]](duration: Duration) =
  const
    # well, we need to be on windows to actually use the timer
    actuallyUseWindowsTimer = defined(windows) and useWindowsTimer

  var
    estimate {.global.} = initDuration(milliseconds = 5)
    mean {.global.} = initDuration(milliseconds = 5)
    m2 {.global.} = 0f
    count {.global.} = 1i64
    durationMutable = duration

  when actuallyUseWindowsTimer:
    var timer {.global.} = CreateWaitableTimer(nil, 0, nil)
    const timeEpsilon = initDuration(nanoseconds = 100)
  else:
    const timeEpsilon = initDuration()

  while durationMutable - estimate > timeEpsilon:
    when actuallyUseWindowsTimer:
      let toWait = durationMutable - estimate
      var due = -int64(toWait.inNanoseconds() div 100)
      let begin = getTime()
      SetWaitableTimerEx(timer, addr due, 0, nil, nil, nil, 0)
      WaitForSingleObject(timer, INFINITE)
      let `end` = getTime()
    else:
      let begin = getTime()
      sleep(1)
      let `end` = getTime()

    let observed = `end` - begin
    durationMutable -= observed

    count += 1
    let delta = observed - mean
    mean += delta div count
    m2   += delta.inFloatSeconds() * (observed - mean).inFloatSeconds()
    let stddev = sqrt(m2 / float(count - 1))
    let (intpart, floatpart) = splitDecimal(stddev)

    estimate = mean + initDuration(seconds = int intpart, nanoseconds = int floatpart * 1e9)

  spinLockSleep(durationMutable)

proc preciseSleep*(duration: Duration) =
  accurateSleep[false](duration)

proc windowsTimerSleep*(duration: Duration) =
  accurateSleep[true](duration)

proc initFixedTimeLoop*(tps, fps: float, window: Window = nil): FixedTimeGameLoop =
  result.window = window
  if not isNil(window):
    result.handleInput = proc() = pollWindowSystemEvents()
  else:
    result.handleInput = proc() = discard
  result.sleep = osSleep
  result.updateDelta = initDuration(nanoseconds = int 1e9 / tps)
  result.renderDelta = initDuration(nanoseconds = int 1e9 / fps)

proc stop*(loop: var GameLoop) =
  loop.stop = true

proc run*(loop: var FixedTimeGameLoop) =
  var prev = getTime()
  var steps = initDuration()
  while not loop.stop:
    let loopStartTime = getTime()
    let elapsed = loopStartTime - prev
    prev = loopStartTime
    steps += elapsed

    loop.handleInput()

    if not isNil(loop.window) and loop.window.shouldClose():
      loop.stop = true

    while steps >= loop.updateDelta:
      loop.updatecb(loop.updateDelta)
      steps -= loop.updateDelta

    loop.rendercb()
    if not isNil(loop.window):
      loop.window.swapBuffers()

    loop.sleep(loopStartTime + loop.renderDelta - getTime())

proc `update=`*(loop: var GameLoop, update: UpdateCallback) =
  loop.updatecb = update

proc `render=`*(loop: var GameLoop, render: RenderCallback) =
  loop.rendercb = render

proc `handleInput=`*(loop: var GameLoop, handleInput: InputHandleCallback) =
  loop.handleInput = handleInput

proc `sleep=`*(loop: var GameLoop, sleep: SleepProc) =
  loop.sleep = sleep

when isMainModule:
  # const millis = 10
  # template measureSleep(blk: untyped): float =
  #   var res = 0f
  #   for i in 0..<1000:
  #     let begin = getTime()
  #     blk
  #     let `end` = getTime()
  #     res += inFloatSeconds `end` - begin - initDuration(milliseconds = millis)
  #   res

  # let ps = measureSleep:
  #   preciseSleep(initDuration(milliseconds = millis))
  # let oss = measureSleep:
  #   osSleep(initDuration(milliseconds = millis))
  # let sps = measureSleep:
  #   spinLockSleep(initDuration(milliseconds = millis))
  # echo oss, " ", sps, " ", ps

  var loop = initFixedTimeLoop(60, 30)
  loop.update = proc (delta: Duration) = echo delta.inFloatSeconds()
  loop.render = proc () = discard
  loop.run()
