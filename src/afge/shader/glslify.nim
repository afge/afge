import os
import macros
import nimterop/build
import strformat
import hashes

when not defined(AFGE_GLOBAL_GLSLIFY):
  const 
    # I won't install nodejs for you, do it yourself
    AFGE_NPM_EXEC {.strdefine.} = "npm"
    AFGE_NODE_EXEC {.strdefine, used.} = "node"
    # aliases
    npm = AFGE_NPM_EXEC
    node = AFGE_NODE_EXEC
    baseDir = getProjectCacheDir("afge")
    glslifyDir = baseDir / "node_modules" / "glslify"
    glslifyExec {.used.} = fmt"{node.sanitizePath} {glslifyDir.sanitizePath}/bin.js"
  static:
    if not dirExists(glslifyDir):
      echo fmt"glslify not found. installing via '{npm}' in {baseDir}"
      let result = execAction(fmt"{npm.sanitizePath} -C {baseDir.sanitizePath} install glslify")
      assert result.ret == 0, fmt"{npm} failed. Return code: {result.ret}"
      echo result.output
else:
  const AFGE_GLOBAL_GLSLIFY {.strdefine.} = ""
  const glslifyExec = AFGE_GLOBAL_GLSLIFY
proc glslifyToGlsl*(glslSource: string): string =
  when defined(nimsuggest) or defined(nimcheck):
    return ""
  else:
    return staticExec(glslifyExec, glslSource, $hash(glslSource))

macro glslify*(code: static[string]): untyped = newLit glslifyToGlsl(code)
macro glslifyFile*(filename: static[string]): untyped = newLit glslifyToGlsl(staticRead(filename))

# suppress warnings
static:
  when defined(nimsuggest) or defined(nimcheck):
    discard sizeof Hash
