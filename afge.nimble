# Package

version       = "0.1.0"
author        = "ngoduyanh"
description   = "Abstraction-free game engine written in Nim"
license       = "MIT"
srcDir        = "afge"


# Dependencies

requires "nim >= 1.0.0"
requires "nimterop >= 0.6.13"
requires "glm >= 1.1.1"

task quadDemo, "run the textured quad demo":
  exec "nim c -o:bin/textured_quad -r demo/textured_quad.nim"
task modelDemo, "run the model demo":
  exec "nim c -o:bin/model -r demo/model.nim"
task g2dDemo, "run the 2D graphics demo":
  exec "nim c -o:bin/g2d -r demo/graphics2d.nim"