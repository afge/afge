proc toBitfield*[E: enum, T](s: set[E], t: typedesc[T]): T =
  for e in s:
    result = result or T ord e