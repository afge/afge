import os, nimterop/build
import tables, pegs, strutils, macros

export build

when not defined(AFGE_DISABLE_GLSLI_STDLIB):
  const AFGE_STDLIB_PATH {.strdefine.} = getProjectCacheDir("afge" / "glsli-stdlib")
  const stdlibDir = AFGE_STDLIB_PATH
  static:
    proc copy(path: string) =
      mkDir((stdlibDir / path).parentDir())
      writeFile(stdlibDir / path, slurp "stdlib" / path)

    copy("3d.glslib")
    copy("animation.glslib")
    copy("layout.glslib")
    copy("r2dapi" / "common.glslib")
    copy("r2dapi" / "vertex.glslib")
    copy("r2dapi" / "fragment.glslib")
    
const quoteIncludePeg = """s <- ws '#' ws 'include' ws '"' {[^"]+} '"' ws
                           comment <- '/*' @ '*/' / '//' .*
                           ws <- (comment / \s+)* """
const angleIncludePeg = """s <- ws '#' ws 'include' ws '<' {[^>]+} '>' ws
                           comment <- '/*' @ '*/' / '//' .*
                           ws <- (comment / \s+)* """
const versionPeg = """s <- ws '#' ws 'version' ws {.*}
                      comment <- '/*' @ '*/' / '//' .*
                      ws <- (comment / \s+)* """               
const AFGE_MAX_INCLUDE_PER_FILE {.intdefine.} = 256
proc findIncludeFile(path: string, searchDirectories: openArray[string]): tuple[exists: bool, includeFilePath, content: string] =
  for dir in searchDirectories:
    let file = dir / path
    if file.fileExists():
      result.exists = true
      result.includeFilePath = file.absolutePath(getProjectDir())
      result.content = file.slurp()
      return

proc preprocessInclude(source, path: string,
                       includePaths: seq[string],
                       defines: seq[string],
                       stripVersion: bool,
                       includeCounts = newTable[string, int]()): string =
  for define in defines:
    result.add "#define " & define & "\n"
  result.add "#line 0 \"" & path & "\"\n"
  var quoteIncludePaths = newSeq[string]()
  let fileDirectory = if path.fileExists(): path.parentDir else: path
  if fileDirectory.dirExists():
    quoteIncludePaths.add fileDirectory
  quoteIncludePaths.add(includePaths)
  var lineNumber = 0
  for line in source.splitLines():
    lineNumber += 1
    var quote: bool
    var includePath: string
    if line =~ peg quoteIncludePeg:
      quote = true
      includePath = matches[0]
    elif line =~ peg angleIncludePeg:
      quote = false
      includePath = matches[0]
    elif line =~ peg versionPeg:
      if not stripVersion:
        result = line & "\n" & result
      continue
    else:
      result.add line
      result.add "\n"
      continue

    let (exists, includeFilePath, content) = 
      findIncludeFile(includePath, if quote: openArray[string] quoteIncludePaths else: includePaths)
    if not exists:
      assert false, "kys"   
    let count = if includeFilePath in includeCounts: includeCounts[includeFilePath] else: 0
    if count >= AFGE_MAX_INCLUDE_PER_FILE:
      assert false, "kys"
    includeCounts[includeFilePath] = count + 1
    result.add "#line 0 \"" & includeFilePath & "\"\n"
    result.add preprocessInclude(content, includeFilePath, includePaths, @[], true, includeCounts)
    result.add "\n#line " & $(lineNumber + 1) & " \"" & path & "\"\n"

macro glsli*(path: static[string], 
             includes: static[string] = "",
             defines: static[string] = "",
             useStdlib: static[bool] = true,
             stripVersion: static[bool] = false): untyped =
  let path = getProjectDir() / path
  let content = slurp path

  var actualIncludes: seq[string]
  var actualDefines: seq[string]
  if includes.len > 0:
    for path in includes.splitLines:
      let path = path.strip()
      if path.len == 0:
        continue
      actualIncludes.add path
  if defines.len > 0:
    for define in defines.splitLines:
      let define = define.strip()
      if define.len == 0:
        continue
      actualDefines.add define
  when not defined(AFGE_DISABLE_GLSLI_STDLIB):
    if useStdlib:
      actualIncludes.add stdlibDir
  let preprocessResult = preprocessInclude(content, path, actualIncludes, actualDefines, stripVersion)
  result = newLit preprocessResult

macro glslis*(source: static[string], 
              includes: static string = "",
              defines: static string = "",
              useStdlib: static[bool] = true,
              stripVersion: static[bool] = false): untyped =
  var actualIncludes: seq[string]
  var actualDefines: seq[string]
  if includes.len > 0:
    for path in includes.splitLines:
      let path = path.strip()
      if path.len == 0:
        continue
      actualIncludes.add path
  if defines.len > 0:
    for define in defines.splitLines:
      let define = define.strip()
      if define.len == 0:
        continue
      actualDefines.add define
  when not defined(AFGE_DISABLE_GLSLI_STDLIB):
    if useStdlib:
      actualIncludes.add stdlibDir
  let preprocessResult = preprocessInclude(source, "<unnamed>", actualIncludes, actualDefines, stripVersion)
  result = newLit preprocessResult