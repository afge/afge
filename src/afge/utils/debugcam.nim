import glm
import ../display/window

type
  DebugCameraKeys* {.pure.} = enum
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT,
    DOWN,
    UP,
    LAST

  DebugCamera = object
    moveSpeed*: float32 # units/second
    turnSpeed*: float32 # radians/second
    position*: Vec3f
    pitch*, yaw*: float32
    # input settings
    keys*: array[ord DebugCameraKeys.LAST, cint]
    pitchMB*, yawMB*: cint
    # input states
    prevX, prevY: cdouble

proc `[]=`*[N, V](keys: var array[N, V], key: DebugCameraKeys, value: V) =
  keys[ord key] = value

proc `[]`*[N, V](keys: var array[N, V], key: DebugCameraKeys): V =
  return keys[ord key]

proc defaultDebugCamera*(): DebugCamera =
  result.moveSpeed = 2.0
  result.turnSpeed = 0.3
  result.keys[DebugCameraKeys.FORWARD] = GLFW_KEY_W
  result.keys[DebugCameraKeys.BACKWARD] = GLFW_KEY_S
  result.keys[DebugCameraKeys.LEFT] = GLFW_KEY_A
  result.keys[DebugCameraKeys.RIGHT] = GLFW_KEY_D
  result.keys[DebugCameraKeys.DOWN] = GLFW_KEY_LEFT_SHIFT
  result.keys[DebugCameraKeys.UP] = GLFW_KEY_SPACE
  result.pitchMB = GLFW_MOUSE_BUTTON_1
  result.yawMB = GLFW_MOUSE_BUTTON_1

proc handleInput*(camera: var DebugCamera, window: Window, delta: float32) =
  const PIf = float32 PI
  var camMoveInput = vec3(0i32)

  if window.getKey(camera.keys[DebugCameraKeys.FORWARD]):
    camMoveInput.z -= 1
  if window.getKey(camera.keys[DebugCameraKeys.BACKWARD]):
    camMoveInput.z += 1
  if window.getKey(camera.keys[DebugCameraKeys.LEFT]):
    camMoveInput.x -= 1
  if window.getKey(camera.keys[DebugCameraKeys.RIGHT]):
    camMoveInput.x += 1
  if window.getKey(camera.keys[DebugCameraKeys.UP]):
    camMoveInput.y += 1
  if window.getKey(camera.keys[DebugCameraKeys.DOWN]):
    camMoveInput.y -= 1

  if camMoveInput != vec3(0i32):
    let scaledCamMoveInput = vec3f(camMoveInput).normalize() * delta * camera.moveSpeed
    camera.position += (mat4(1.0f32).rotateY(-camera.yaw) * vec4(scaledCamMoveInput, 1.0)).xyz 

  let (x, y) = window.getCursorPosition()
  if window.getMouseButton(camera.pitchMB):
    let dy = float32(y - camera.prevY) * delta * camera.turnSpeed
    camera.pitch = floorMod(camera.pitch + dy + PIf, 2 * PIf) - PIf

  if window.getMouseButton(camera.yawMB):
    let dx = float32(x - camera.prevX) * delta * camera.turnSpeed
    camera.yaw = floorMod(camera.yaw + dx, 2 * PIf)

  camera.prevX = x
  camera.prevY = y

proc toMatrix*(camera: DebugCamera): Mat4f =
  return mat4(1.0f32).rotateX(camera.pitch).rotateY(camera.yaw).translate(-camera.position)
