
import math
import tables
import options

import glm
import ../utils/span
import ../wrapper/assimp

type
  KeyFrame*[D] = object
    timestamp*: float
    data*: D
  Bone* = object
    transform*: Mat4f
    id*: int
  BoneTable* = Table[string, Bone]

  KeyPosition* = KeyFrame[Vec3f]
  KeyRotation* = KeyFrame[Quatf]
  KeyScale* = KeyFrame[Vec3f]
  AnimatedBone* = object
    keyPositions*: seq[KeyPosition]
    keyRotations*: seq[KeyRotation]
    keyScales*: seq[KeyPosition]
    name*: string
    bone*: Bone

  MeshAnimationNode* = object
    transform*: Mat4f
    name*: string
    children*: seq[MeshAnimationNode]
  MeshAnimation* = object
    duration*: float
    ticksPerSecond*: float
    bones*: Table[string, Bone]
    animBones*: seq[AnimatedBone]
    rootNode*: MeshAnimationNode
  MeshAnimator* = object
    currentTime*: float
    animation*: MeshAnimation

proc interpolate(x, y: Vec3f, factor: float): auto = mix(x, y, float32 factor)
proc interpolate(x, y: Quatf, factor: float): auto = slerp(x, y, float32 factor)
proc getInterpolatedData[D](frames: openArray[KeyFrame[D]], animationTime: float): auto =
  if frames.len <= 1:
    assert frames.len == 1
    return frames[0].data
  for i in 0 ..< frames.len - 1:
    if frames[i + 1].timestamp >= animationTime:
      let timePassed = animationTime - frames[i].timestamp
      let totalTime = frames[i + 1].timestamp - frames[i].timestamp
      let factor = timePassed / totalTime
      return interpolate(frames[i].data, frames[i + 1].data, factor)
  assert false
proc update*(animBone: var AnimatedBone, animationTime: float) =
  let position = getInterpolatedData(animBone.keyPositions, animationTime)
  let rotation = getInterpolatedData(animBone.keyRotations, animationTime).normalize()
  let scale = getInterpolatedData(animBone.keyScales, animationTime)

  animBone.bone.transform = poseMatrix(position, rotation, scale)
proc newAnimator*(animation: MeshAnimation): auto = MeshAnimator(currentTime: 0.0f, animation: animation)

proc update*(animator: var MeshAnimator, delta: float, matrices: Span[Mat4f]) =
  if delta == 0.0:
    return
  var tps = animator.animation.ticksPerSecond
  if tps == 0.0f:
    tps = 1.0f

  animator.currentTime = (animator.currentTime + delta * tps) mod animator.animation.duration

  proc calculateBoneTransform(node: var MeshAnimationNode, animator: var MeshAnimator, parentTransform: Mat4f = mat4f(1.0)) =
    var nodeTransform = node.transform
    for animBone in animator.animation.animBones.mitems:
      if animBone.name == node.name:
        animBone.update(animator.currentTime)
        nodeTransform = animBone.bone.transform

    let globalTransform = parentTransform * nodeTransform
    if animator.animation.bones.hasKey(node.name):
      let bone = animator.animation.bones[node.name]
      matrices[bone.id] = globalTransform * bone.transform
    
    for child in node.children.mitems:
      calculateBoneTransform(child, animator, globalTransform)
  calculateBoneTransform(animator.animation.rootNode, animator)
