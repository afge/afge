# afge - Abstract-free game engine

A game engine designed with no abstractions in mind, written in [Nim](https://github.com/nim-lang/Nim). Works only on desktop machines with OpenGL 3.3+.

Since the engine has no abstractions, you are in power. You can use all of the APIs provided, both low-level (glw - OpenGL wrapper API) and high-level (model, renderer2d/r2d, etc.). The problem is that you need to know what is happening inside the API, and the only way to do it is reading the source code (or the documentation if it ever comes out). This is why I'm probably the only person using this to make games.

Note: These APIs are more like tools/libraries than actual API, since you can make a identical API with the same performance (if not better). They are used oftenly, don't take up much space (Nim compiler won't care about files that are not imported).

> I'm currently working on graphics first, so there are no support for other things like sound and networking. Input handling is possible but requires knowledge of the GLFW library.

## Why Nim?

Nim is a programming language that give a lot of power to the programmer, both the afge dev and the game dev. A lot of APIs are made possible by Nim's macro system. Nim is also able to compile to C, and therefore its C interop is very good. In order for native (C/C++) library to be cloned and built, the wrapper file must be imported, either directly or indirectly.

## Window API

afge uses [GLFW](https://github.com/glfw/glfw) to manage windows, monitors. GLFW bindings (afge/wrapper/glfw) and a semi-high-level API (afge/display/window) (just a typesafe wrapper without GLFW prefixes) are available. 

## Graphics related APIs

afge uses OpenGL for graphics, and there's no OpenGL ES, WebGL or Vulkan support, so mobile/web targets are not supported.

The core graphics API is glw (OpenGL wrapper, afge/wrapper/gl), but directly using the gl API (afge/wrapper/gl), you have total access to all extensions and OpenGL functions.

The high-level APIs are the mesh API, it still requires some glw code to works with, the model API, which is pretty simple to use and have support for skeletal animation, and the 2D graphics API (renderer2d API).

If you are using any rendering API that is not the renderer2d API, you'll need a shader. There are several ways to write shaders: write ordinary GLSL with fmt-like brackets ("${abc}"), #include directives or with dependencies from [glslify](https://github.com/glslify/glslify). You can also write shaders in [circle-lang](https://github.com/seanbaxter/circle) (a C++-like language), but the language is experimental.

A shader standard library is only available for glsli (GLSL + #include), so it's the best way to write shaders in the meantime.

Note: afge is primary designed for modern GPUs which has some extensions like [GL_ARB_direct_state_access](https://www.khronos.org/registry/OpenGL/extensions/ARB/ARB_direct_state_access.txt) or [GL_ARB_bindless_texture](https://www.khronos.org/registry/OpenGL/extensions/ARB/ARB_bindless_texture.txt) (pretty popular, most NVIDIA/AMD GPUs both supports this), so for old/integrated GPUs, things may be a little bit slow (you still can do your own raw GL calls for these platforms btw).

## License

afge is licensed under the MIT license (cuck license since this is a game engine).