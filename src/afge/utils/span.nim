import strformat
import ../layout/layoututils
import glm

type
  Span*[T] = object
    data*: ptr T
    len*: int

proc newSpan*[T](data: ptr T, len: int) : Span[T] =
  result = Span[T](data: data, len: len)

proc `[]`*(span: Span, index: int): Span.T =
  if index in 0 ..< span.len:
    return cast[ptr Span.T](cast[ByteAddress](span.data) + index * luSizeof Span.T)[]
  else:
    raise IndexDefect.newException(fmt"{index} is not in 0 ..< {span.len}")

proc `[]`*(span: var Span, index: int): var Span.T =
  if index in 0 ..< span.len:
    return cast[ptr Span.T](cast[ByteAddress](span.data) + index * luSizeof Span.T)[]
  else:
    raise IndexDefect.newException(fmt"{index} is not in 0 ..< {span.len}")

proc `[]=`*(span: Span, index: int, value: Span.T) =
  if index in 0 ..< span.len:
    cast[ptr Span.T](cast[ByteAddress](span.data) + index * luSizeof Span.T)[] = value
  else:
    raise IndexDefect.newException(fmt"{index} is not in 0 ..< {span.len}")

iterator items*(span: Span): Span.T =
  for i in 0 ..< span.len:
    yield span[i]

when isMainModule:
  var x = [1, 2, 3, 4]
  let span = newSpan(addr x[0], len x)
  echo typeof span
  span[2] = 10
  echo x
