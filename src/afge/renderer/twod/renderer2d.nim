import strformat, tables, hashes, sets, options
import glm
import ../../utils/[own, span]
import ../../graphics/[glw, shader_utils]
import ../../shader/glsli
import ../../graphics/mesh

export glm

const
  AFGE_R2D_MAX_TEXTURES {.intdefine.} = 16
  R2D_MAX_TEXTURES = max(AFGE_R2D_MAX_TEXTURES, 1)

  AFGE_R2D_PAINT_DATA_MAX_SIZE {.intdefine.} = 4 # = 4 * sizeof Vec4f bytes
  R2D_PAINT_DATA_MAX_SIZE = max(AFGE_R2D_PAINT_DATA_MAX_SIZE, 2)

  AFGE_R2D_MAX_PAINTS {.intdefine.} = 128
  R2D_MAX_PAINTS = max(AFGE_R2D_MAX_PAINTS, 1)
type
  Renderer2DTexturingMode* {.pure.} = enum
    SAMPLER2D_ARRAY
    BINDLESS_TEXTURE
  Vertex* = object
    pos*: Vec2f
    tcoords*: Vec2f
    color*: Vec4f
    paintId*: int32
  Renderer2DPaintType = enum
    SIMPLE
    TEXTURE
  Renderer2DPaint* = object
    paintType: float32
    textureBinding: float32
    texture: GLuint
    unused: float32
    data*: array[R2D_PAINT_DATA_MAX_SIZE - 1, Vec4f]


  Renderer2D* = object
    mesh: Mesh
    vertexBuffer: TypedBuffer[Vertex]
    uniformBuffer: Buffer
    program: Own[ShaderProgram]
    texturingMode: Renderer2DTexturingMode
    vertexBufferSize: int
    mappedVertexBuffer: Span[Vertex]
    mappedVertexBufferPtr: int
    mappedVertexBufferSeq: seq[Vertex] # used when vertexBufferSize < 0
    mappedUniformBuffer: pointer
    textureCache: Table[GLuint, int32]
    paintCache: Table[Renderer2DPaint, int32]
    currentTextureId: int32
    
proc hash(p: Renderer2DPaint): Hash =
  for v in p.data:
    result = result !& v.x.hash !& v.y.hash !& v.z.hash !& v.w.hash
  result = !$result

# ordered by best to worst
proc getAvailableTexturingModes*(): seq[Renderer2DTexturingMode] =
  if GLAD_GL_ARB_bindless_texture and GLAD_GL_NV_gpu_shader5:
    result.add Renderer2DTexturingMode.BINDLESS_TEXTURE
  
  result.add Renderer2DTexturingMode.SAMPLER2D_ARRAY

const defines = fmt"""
AFGE_R2D_MAX_TEXTURES {R2D_MAX_TEXTURES}
AFGE_R2D_PAINT_DATA_VEC4S {R2D_PAINT_DATA_MAX_SIZE}
AFGE_R2D_MAX_PAINTS {R2D_MAX_PAINTS}
"""
const 
  vertexShader = glslis("#version 330\n#include <r2dapi/vertex.glslib>", defines = defines)
  fragmentShader = glslis("#version 330\n#include <r2dapi/fragment.glslib>", defines = defines)

proc initRenderer2D*(program: Own[ShaderProgram] = notOwn(default(ShaderProgram)),
                     texturingMode = getAvailableTexturingModes()[0],
                     vertexBufferSize: int = -1): Renderer2D =
  result.mesh = createMesh()
  result.vertexBuffer = result.mesh.addTypedBuffer(Vertex)
  result.uniformBuffer = createGL(Buffer)
  result.program = program
  result.texturingMode = texturingMode
  result.vertexBufferSize = vertexBufferSize

  var runtimeShaderMacros = newSeq[string]()
  if texturingMode == Renderer2DTexturingMode.BINDLESS_TEXTURE:
    runtimeShaderMacros.add "AFGE_R2D_BINDLESS_TEXTURE"
  if program.value.GLuint == 0:
    let vertexShader = vertexShader.glslWithMacros(runtimeShaderMacros)
    let fragmentShader = fragmentShader.glslWithMacros(runtimeShaderMacros)
    let defaultProgram = createShaderProgram(
      {"vPos": 0, "vTexCoords": 1, "vColor": 2, "vPaintId": 3},
      (ShaderType.VERTEX, vertexShader),
      (ShaderType.FRAGMENT, fragmentShader),
    ).getProgram()
    result.program = own defaultProgram

  glUniformBlockBinding(GLuint result.program.value, glGetUniformBlockIndex(GLuint result.program.value, "UniformBlock"), 0)
  result.program.value.use()
  for i in 0 ..< AFGE_R2D_MAX_TEXTURES:
    discard result.program.value.setUniform("tex" & $i, int32(i + 1))

  if vertexBufferSize > 0:
    if GLAD_GL_ARB_buffer_storage:
      result.vertexBuffer.allocDataImmutable(vertexBufferSize)
    else:
      result.vertexBuffer.setBufferData(nil, vertexBufferSize, BufferUsage.DYNAMIC_DRAW)
      result.mappedVertexBufferSeq = newSeq[Vertex]()

  var uniformBufferSize = sizeof(Mat4f) + R2D_MAX_PAINTS * sizeof(Renderer2DPaint)
  if texturingMode == BINDLESS_TEXTURE:
    uniformBufferSize += R2D_MAX_TEXTURES * sizeof(GLuint64)

  if uniformBufferSize > 0:
    if GLAD_GL_ARB_buffer_storage:
      result.uniformBuffer.allocDataImmutable(uniformBufferSize)
    else:
      result.uniformBuffer.setBufferData(nil, uniformBufferSize, BufferUsage.DYNAMIC_DRAW)

proc mapBuffers(r2d: var Renderer2D) =
  if r2d.vertexBufferSize < 0:
    r2d.mappedVertexBufferSeq.setLen(0)
  else:
    r2d.mappedVertexBuffer = r2d.vertexBuffer.mapTypedSpan(nonDSABindTarget = GL_ARRAY_BUFFER)
    r2d.mappedVertexBufferPtr = 0
  r2d.mappedUniformBuffer = r2d.uniformBuffer.map(nonDSABindTarget = GL_UNIFORM_BUFFER)

  r2d.mappedVertexBufferPtr = 0
  r2d.textureCache.clear()
  r2d.paintCache.clear()
  r2d.currentTextureId = 0

proc flush(r2d: var Renderer2D) =
  var count = 0

  if r2d.vertexBufferSize < 0:
    r2d.vertexBuffer.setBufferData(r2d.mappedVertexBufferSeq)
    count = r2d.mappedVertexBufferSeq.len
  else:
    discard r2d.vertexBuffer.unmap(nonDSABindTarget = GL_ARRAY_BUFFER)
    count = r2d.mappedVertexBufferPtr
  discard r2d.uniformBuffer.unmap(nonDSABindTarget = GL_UNIFORM_BUFFER)

  r2d.program.value.use()
  r2d.mesh.bindGL()
  r2d.uniformBuffer.bindUniformBuffer(0)
  if r2d.texturingMode == Renderer2DTexturingMode.BINDLESS_TEXTURE:
    for tex in r2d.textureCache.keys:
      glMakeTextureHandleResidentARB(glGetTextureHandleARB(tex))
  drawArrays(DrawType.TRIANGLE, 0, count)
  if r2d.texturingMode == Renderer2DTexturingMode.BINDLESS_TEXTURE:
    for tex in r2d.textureCache.keys:
      glMakeTextureHandleNonResidentARB(glGetTextureHandleARB(tex))

proc insertTexture(r2d: var Renderer2D, texture: GLuint): int32 =
  if texture == 0:
    return -1
  if texture in r2d.textureCache:
    return r2d.textureCache[texture]

  if r2d.currentTextureId >= R2D_MAX_TEXTURES:
    r2d.flush()
    r2d.mapBuffers()

  result = r2d.currentTextureId
  r2d.textureCache[texture] = result
  case r2d.texturingMode:
  of BINDLESS_TEXTURE: 
    let handle = glGetTextureHandleARB(texture)
    let uboOffset = sizeof(Mat4f) + sizeof(Renderer2DPaint) * R2D_MAX_PAINTS + 
                                    sizeof(uint64) * r2d.currentTextureId
    cast[ptr GLuint64](cast[ByteAddress](r2d.mappedUniformBuffer) + uboOffset)[] = handle
  of SAMPLER2D_ARRAY: 
    # first binding is reserved for the uniform buffer
    Texture2D(texture).bindTex(r2d.currentTextureId + 1)

  r2d.currentTextureId += 1

proc beginFrame*(r2d: var Renderer2D, width, height: float32) =
  r2d.mapBuffers()
  cast[ptr Mat4f](r2d.mappedUniformBuffer)[] = ortho(0.0f32, width, height, 0.0f32, -1.0f32, 1.0f32)

proc endFrame*(r2d: var Renderer2D) =
  r2d.flush()

proc addTriangle(r2d: var Renderer2D, vertices: openArray[Vertex]) =
  assert vertices.len <= 3
  if r2d.vertexBufferSize < 0:
    r2d.mappedVertexBufferSeq.add(vertices)
  else:
    if r2d.mappedVertexBufferPtr + 2 >= r2d.vertexBufferSize:
      r2d.flush()
      r2d.mapBuffers()
    for v in vertices:
      r2d.mappedVertexBuffer[r2d.mappedVertexBufferPtr] = v
      r2d.mappedVertexBufferPtr += 1

proc addShape*(r2d: var Renderer2D, paint: Renderer2DPaint, vertices: varargs[Vertex]) =
  assert vertices.len %% 3 == 0
  var paintId = -1
  var paint = paint
  paint.textureBinding = float32 r2d.insertTexture(paint.texture)
  
  if paint in r2d.paintCache:
    paintId = r2d.paintCache[paint]
  else:
    if r2d.paintCache.len >= R2D_MAX_PAINTS:
      r2d.flush()
      r2d.mapBuffers()

    paintId = r2d.paintCache.len
    var paintOffset = sizeof(Mat4f) + paintId * sizeof paint
    var dstPaintAddr = cast[pointer](cast[ByteAddress](r2d.mappedUniformBuffer) + paintOffset)
    cast[ptr Renderer2DPaint](dstPaintAddr)[] = paint
    r2d.paintCache[paint] = int32 paintId

  var i = 0
  while i < vertices.len:
    var triangleVertices = vertices[i .. i + 2]
    for v in triangleVertices.mitems:
      v.paintId = int32 paintId
    r2d.addTriangle(triangleVertices)
    i += 3

proc simplePaint*(): Renderer2DPaint =
  result.paintType = float32 ord Renderer2DPaintType.SIMPLE

proc texturePaint*(texture: Texture2D): Renderer2DPaint =
  result.paintType = float32 ord Renderer2DPaintType.TEXTURE
  result.texture = GLuint texture

# high-level API
type
  ColorArray[N: static[int], C: static[ColorComponent], T: SomeNumber] = array[N, Color[C, T]]
  ColorSeq[C: static[ColorComponent], T: SomeNumber] = seq[Color[C, T]]
  PaintArg = Renderer2DPaint | Color | ColorArray | ColorSeq

proc toRGBAF32Vector(c: Color): Vec4f =
  when Color.C == ColorComponent.RGBA and Color.T is float32:
    result = Vec4f(arr: c.arr)
  else:
    result = Vec4f(arr: convertColor[ColorComponent.RGBA, float32, Color.C, Color.T](c).arr)

proc color(p: PaintArg, idx: int): Vec4f =
  when p is Color:
    return p.toRGBAF32Vector()
  elif p is ColorArray or p is ColorSeq:
    if p.len > 0:
      return p[min(idx, p.len - 1)].toRGBAF32Vector()

  return COLOR_WHITE.toRGBAF32Vector()

proc paint(p: PaintArg): Renderer2DPaint =
  when p is Renderer2DPaint:
    return p
  else:
    return simplePaint()

proc rect*(r2d: var Renderer2D,
           x, y, w, h: float32,
           paint: PaintArg,
           tx = 0.0f32, ty = 0.0f32, tw = 1.0f32, th = 1.0f32) =
  let
    v0 = Vertex(pos: vec2(x, y), tcoords: vec2(tx, ty + th), color: paint.color(0))
    v1 = Vertex(pos: vec2(x + w, y), tcoords: vec2(tx + tw, ty + th), color: paint.color(1))
    v2 = Vertex(pos: vec2(x + w, y + h), tcoords: vec2(tx + tw, ty), color: paint.color(2))
    v3 = Vertex(pos: vec2(x, y + h), tcoords: vec2(tx, ty), color: paint.color(3))
  addShape(r2d, paint.paint(), v0, v2, v1, v3, v2, v0)


proc destroy*(r2d: var Renderer2D) =
  r2d.mesh.destroy()
  r2d.uniformBuffer.destroy()
  r2d.program.ifOwnVoid(proc (p: ShaderProgram) = p.destroy())
