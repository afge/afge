proc exec*(cmd: string, cache: string = "") {.used.} =
  when not defined(nimsuggest) and not defined(nimcheck):
    let (output, ret) = gorgeEx(cmd, cache = cache)
    if output != "":
      echo output
    assert ret == 0
