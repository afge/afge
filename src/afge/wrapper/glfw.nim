import nimterop/[build, cimport]

const GLFW3_GIT_URL {.strdefine.} = "https://github.com/glfw/glfw"

setDefines(@["glfw3Git", "glfw3Static"])
getHeader(
  "glfw3.h",
  giturl = GLFW3_GIT_URL,
  conanuri = "glfw/$1",
  outdir = getProjectCacheDir("afge" / "glfw"),
  cmakeFlags = "-DGLFW_BUILD_TESTS=OFF -DGLFW_BUILD_DOCS=OFF -DGLFW_BUILD_EXAMPLES=OFF",
  altNames = "glfw|glfw3"
)

cPlugin:
  proc onSymbol*(sym: var Symbol) {.exportc, dynlib.} =
    if sym.name == "GLFW_CURSOR":
      sym.name = "GLFW_CURSOR_MODE" & sym.override
      
    
when defined(unix):
  cPassL("-lm")
  cPassL("-lpthread")

when not isDefined(glfw3Static):
  cImport(glfw3Path, recurse = true, dynlib = "glfw3LPath")
else:
  cImport(glfw3Path, recurse = true)
