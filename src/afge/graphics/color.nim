import math, strutils

import glm
import ../wrapper/stb

type
  ColorComponent* {.pure.} = enum
    GRAY = STBI_grey,
    GRAY_ALPHA = STBI_grey_alpha
    RGB = STBI_rgb,
    RGBA = STBI_rgb_alpha,
    RED,
    RED_GREEN,
    RED_ALPHA

func dimensions(comp: ColorComponent): int =
  result = case comp:
  of GRAY, RED: 1
  of RED_GREEN, RED_ALPHA, GRAY_ALPHA: 2
  of RGB: 3
  of RGBA: 4

type
  Color*[C: static[ColorComponent], T: SomeNumber] = object
    arr*: array[dimensions C, T]

template r*[C, T](color: Color[C, T]): T =
  color.arr[0]

template `r=`*[C, T](color: var Color[C, T], value: T) =
  color.arr[0] = value

template g*[C, T](color: Color[C, T]): T =
  when C == ColorComponent.RED or C == ColorComponent.RED_ALPHA:
    0
  elif C == ColorComponent.GRAY or C == ColorComponent.GRAY_ALPHA:
    color.r
  else:
    color.arr[1]

template `g=`*[C, T](color: var Color[C, T], value: T) =
  when C == ColorComponent.GRAY or C == ColorComponent.GRAY_ALPHA:
    color.r = value
  elif C == ColorComponent.RGB or C == ColorComponent.RGBA or C == ColorComponent.RED_GREEN:
    color.arr[1] = value

template b*[C, T](color: Color[C, T]): T=
  when C == ColorComponent.RED or C == ColorComponent.RED_ALPHA or C == ColorComponent.RED_GREEN:
    0
  elif C == ColorComponent.GRAY or C == ColorComponent.GRAY_ALPHA:
    color.r
  else:
    color.arr[2]

template `b=`*[C, T](color: var Color[C, T], value: T) =
  when C == ColorComponent.GRAY or C == ColorComponent.GRAY_ALPHA:
    color.r = value
  elif C == ColorComponent.RGB or C == ColorComponent.RGBA:
    color.arr[2] = value

template a*[C, T](color: Color[C, T]): T =
  when C == ColorComponent.RGBA:
    color.arr[3]
  elif C == ColorComponent.RED_ALPHA:
    color.arr[1]
  else:
    when T is SomeInteger:
      255
    else:
      1.0

template `a=`*[C, T](color: var Color[C, T], value: T) =
  when C == ColorComponent.RED_ALPHA or C == ColorComponent.GRAY_ALPHA:
    color.arr[1] = value
  elif C == ColorComponent.RGBA:
    color.arr[3] = value

proc convertColorValue[T1: SomeNumber, T2: SomeNumber](fromValue: T1): T2 =
  when (T1 is SomeInteger) == (T2 is SomeInteger):
    result = T2 fromValue
  elif T1 is SomeFloat:
    result = round(fromValue * 255)
  elif T2 is SomeFloat:
    result = T2(fromValue) / 255

proc convertColor*[C2, T2, C1, T1](color: Color[C1, T1]): Color[C2, T2] =
  result.r = convertColorValue[T1, T2] color.r
  result.g = convertColorValue[T1, T2] color.g
  result.b = convertColorValue[T1, T2] color.b
  result.a = convertColorValue[T1, T2] color.a

proc defaultAlpha*[T: SomeNumber](t: typedesc[T]) = T:
  when T is SomeInteger:
    return 255
  else:
    return 1.0

proc vec1*[T](x: T): Vec[1, T] =
  return Vec[1, T](arr: [x])

proc gray*[T](gray: T): auto =
  return Color[ColorComponent.GRAY, T](arr: [gray])
proc gray*[T](gray: T, alpha: T): auto =
  return Color[ColorComponent.GRAY_ALPHA, T](arr: [gray, alpha])
proc red*[T](red: T): auto =
  return Color[ColorComponent.RED, T](arr: [red])
proc red*[T](red: T, alpha: T): auto =
  return Color[ColorComponent.RED_ALPHA, T](arr: [red, alpha])
proc rg*[T](red: T, green: T): auto =
  return Color[ColorComponent.RED_GREEN, T](arr: [red, green])
proc rgb*[T](red: T, green: T, blue: T): auto =
  return Color[ColorComponent.RGB, T](arr: [red, green, blue])
proc rgba*[T](red: T, green: T, blue:T, alpha: T): auto =
  return Color[ColorComponent.RGBA, T](arr: [red, green, blue, alpha])

proc hex*[C, T](value: string, color: var Color[C, T], offset: int = 0, length: int = -1): int =
  var length = if length < 0: value.len else: length
  var offset = offset

  if value[offset] == '#':
    offset += 1
    length -= 1
    result += 1

  var digits = newSeq[uint8]()
  for i in offset ..< offset + length:
    if value[i] in Digits:
      digits.add (uint8(value[i]) - uint8('0'))
      if digits.len >= 8:
        break
    else:
      break
  
  case digits.len:
  of 3, 4: 
    color.r = digits[0] * 0x11
    color.g = digits[1] * 0x11
    color.b = digits[2] * 0x11
    if digits.len == 4:
      color.a = digits[3] * 0x11
    result += digits.len
  of 6, 8:
    color.r = digits[0] * 0x10 + digits[1]
    color.g = digits[2] * 0x10 + digits[3]
    color.b = digits[4] * 0x10 + digits[5]
    if digits.len == 8:
      color.a = digits[6] * 0x10 + digits[7]
    result += digits.len
  else:
    return 0

# exception version, useful if value is compile-time valid
proc hex*(value: string, offset: int = 0, length: int = 0): Color[ColorComponent.RGBA, uint8] =
  let realLength = if length <= 0: value.len else: length
  if realLength > 0 and hex[ColorComponent.RGBA, uint8](value, result, offset, realLength) != realLength:
    raise Exception.newException("color parsing error")

when isMainModule:
  echo hex("#12345678").arr

const
  COLOR_ALICEBLUE* = rgb[uint8](240, 248, 255)
  COLOR_ANTIQUEWHITE* = rgb[uint8](250, 235, 215)
  COLOR_AQUA* = rgb[uint8]( 0, 255, 255)
  COLOR_AQUAMARINE* = rgb[uint8](127, 255, 212)
  COLOR_AZURE* = rgb[uint8](240, 255, 255)
  COLOR_BEIGE* = rgb[uint8](245, 245, 220)
  COLOR_BISQUE* = rgb[uint8](255, 228, 196)
  COLOR_BLACK* = rgb[uint8]( 0, 0, 0)
  COLOR_BLANCHEDALMOND* = rgb[uint8](255, 235, 205)
  COLOR_BLUE* = rgb[uint8]( 0, 0, 255)
  COLOR_BLUEVIOLET* = rgb[uint8](138, 43, 226)
  COLOR_BROWN* = rgb[uint8](165, 42, 42)
  COLOR_BURLYWOOD* = rgb[uint8](222, 184, 135)
  COLOR_CADETBLUE* = rgb[uint8]( 95, 158, 160)
  COLOR_CHARTREUSE* = rgb[uint8](127, 255, 0)
  COLOR_CHOCOLATE* = rgb[uint8](210, 105, 30)
  COLOR_CORAL* = rgb[uint8](255, 127, 80)
  COLOR_CORNFLOWERBLUE* = rgb[uint8](100, 149, 237)
  COLOR_CORNSILK* = rgb[uint8](255, 248, 220)
  COLOR_CRIMSON* = rgb[uint8](220, 20, 60)
  COLOR_CYAN* = rgb[uint8]( 0, 255, 255)
  COLOR_DARKBLUE* = rgb[uint8]( 0, 0, 139)
  COLOR_DARKCYAN* = rgb[uint8]( 0, 139, 139)
  COLOR_DARKGOLDENROD* = rgb[uint8](184, 134, 11)
  COLOR_DARKGRAY* = rgb[uint8](169, 169, 169)
  COLOR_DARKGREEN* = rgb[uint8]( 0, 100, 0)
  COLOR_DARKGREY* = rgb[uint8](169, 169, 169)
  COLOR_DARKKHAKI* = rgb[uint8](189, 183, 107)
  COLOR_DARKMAGENTA* = rgb[uint8](139, 0, 139)
  COLOR_DARKOLIVEGREEN* = rgb[uint8]( 85, 107, 47)
  COLOR_DARKORANGE* = rgb[uint8](255, 140, 0)
  COLOR_DARKORCHID* = rgb[uint8](153, 50, 204)
  COLOR_DARKRED* = rgb[uint8](139, 0, 0)
  COLOR_DARKSALMON* = rgb[uint8](233, 150, 122)
  COLOR_DARKSEAGREEN* = rgb[uint8](143, 188, 143)
  COLOR_DARKSLATEBLUE* = rgb[uint8]( 72, 61, 139)
  COLOR_DARKSLATEGRAY* = rgb[uint8]( 47, 79, 79)
  COLOR_DARKSLATEGREY* = rgb[uint8]( 47, 79, 79)
  COLOR_DARKTURQUOISE* = rgb[uint8]( 0, 206, 209)
  COLOR_DARKVIOLET* = rgb[uint8](148, 0, 211)
  COLOR_DEEPPINK* = rgb[uint8](255, 20, 147)
  COLOR_DEEPSKYBLUE* = rgb[uint8]( 0, 191, 255)
  COLOR_DIMGRAY* = rgb[uint8](105, 105, 105)
  COLOR_DIMGREY* = rgb[uint8](105, 105, 105)
  COLOR_DODGERBLUE* = rgb[uint8]( 30, 144, 255)
  COLOR_FIREBRICK* = rgb[uint8](178, 34, 34)
  COLOR_FLORALWHITE* = rgb[uint8](255, 250, 240)
  COLOR_FORESTGREEN* = rgb[uint8]( 34, 139, 34)
  COLOR_FUCHSIA* = rgb[uint8](255, 0, 255)
  COLOR_GAINSBORO* = rgb[uint8](220, 220, 220)
  COLOR_GHOSTWHITE* = rgb[uint8](248, 248, 255)
  COLOR_GOLD* = rgb[uint8](255, 215, 0)
  COLOR_GOLDENROD* = rgb[uint8](218, 165, 32)
  COLOR_GRAY* = rgb[uint8](128, 128, 128)
  COLOR_GREY* = rgb[uint8](128, 128, 128)
  COLOR_GREEN* = rgb[uint8]( 0, 128, 0)
  COLOR_GREENYELLOW* = rgb[uint8](173, 255, 47)
  COLOR_HONEYDEW* = rgb[uint8](240, 255, 240)
  COLOR_HOTPINK* = rgb[uint8](255, 105, 180)
  COLOR_INDIANRED* = rgb[uint8](205, 92, 92)
  COLOR_INDIGO* = rgb[uint8]( 75, 0, 130)
  COLOR_IVORY* = rgb[uint8](255, 255, 240)
  COLOR_KHAKI* = rgb[uint8](240, 230, 140)
  COLOR_LAVENDER* = rgb[uint8](230, 230, 250)
  COLOR_LAVENDERBLUSH* = rgb[uint8](255, 240, 245)
  COLOR_LAWNGREEN* = rgb[uint8](124, 252, 0)
  COLOR_LEMONCHIFFON* = rgb[uint8](255, 250, 205)
  COLOR_LIGHTBLUE* = rgb[uint8](173, 216, 230)
  COLOR_LIGHTCORAL* = rgb[uint8](240, 128, 128)
  COLOR_LIGHTCYAN* = rgb[uint8](224, 255, 255)
  COLOR_LIGHTGOLDENRODYELLOW* = rgb[uint8](250, 250, 210)
  COLOR_LIGHTGRAY* = rgb[uint8](211, 211, 211)
  COLOR_LIGHTGREEN* = rgb[uint8](144, 238, 144)
  COLOR_LIGHTGREY* = rgb[uint8](211, 211, 211)
  COLOR_LIGHTPINK* = rgb[uint8](255, 182, 193)
  COLOR_LIGHTSALMON* = rgb[uint8](255, 160, 122)
  COLOR_LIGHTSEAGREEN* = rgb[uint8]( 32, 178, 170)
  COLOR_LIGHTSKYBLUE* = rgb[uint8](135, 206, 250)
  COLOR_LIGHTSLATEGRAY* = rgb[uint8](119, 136, 153)
  COLOR_LIGHTSLATEGREY* = rgb[uint8](119, 136, 153)
  COLOR_LIGHTSTEELBLUE* = rgb[uint8](176, 196, 222)
  COLOR_LIGHTYELLOW* = rgb[uint8](255, 255, 224)
  COLOR_LIME* = rgb[uint8]( 0, 255, 0)
  COLOR_LIMEGREEN* = rgb[uint8]( 50, 205, 50)
  COLOR_LINEN* = rgb[uint8](250, 240, 230)
  COLOR_MAGENTA* = rgb[uint8](255, 0, 255)
  COLOR_MAROON* = rgb[uint8](128, 0, 0)
  COLOR_MEDIUMAQUAMARINE* = rgb[uint8](102, 205, 170)
  COLOR_MEDIUMBLUE* = rgb[uint8]( 0, 0, 205)
  COLOR_MEDIUMORCHID* = rgb[uint8](186, 85, 211)
  COLOR_MEDIUMPURPLE* = rgb[uint8](147, 112, 219)
  COLOR_MEDIUMSEAGREEN* = rgb[uint8]( 60, 179, 113)
  COLOR_MEDIUMSLATEBLUE* = rgb[uint8](123, 104, 238)
  COLOR_MEDIUMSPRINGGREEN* = rgb[uint8]( 0, 250, 154)
  COLOR_MEDIUMTURQUOISE* = rgb[uint8]( 72, 209, 204)
  COLOR_MEDIUMVIOLETRED* = rgb[uint8](199, 21, 133)
  COLOR_MIDNIGHTBLUE* = rgb[uint8]( 25, 25, 112)
  COLOR_MINTCREAM* = rgb[uint8](245, 255, 250)
  COLOR_MISTYROSE* = rgb[uint8](255, 228, 225)
  COLOR_MOCCASIN* = rgb[uint8](255, 228, 181)
  COLOR_NAVAJOWHITE* = rgb[uint8](255, 222, 173)
  COLOR_NAVY* = rgb[uint8]( 0, 0, 128)
  COLOR_OLDLACE* = rgb[uint8](253, 245, 230)
  COLOR_OLIVE* = rgb[uint8](128, 128, 0)
  COLOR_OLIVEDRAB* = rgb[uint8](107, 142, 35)
  COLOR_ORANGE* = rgb[uint8](255, 165, 0)
  COLOR_ORANGERED* = rgb[uint8](255, 69, 0)
  COLOR_ORCHID* = rgb[uint8](218, 112, 214)
  COLOR_PALEGOLDENROD* = rgb[uint8](238, 232, 170)
  COLOR_PALEGREEN* = rgb[uint8](152, 251, 152)
  COLOR_PALETURQUOISE* = rgb[uint8](175, 238, 238)
  COLOR_PALEVIOLETRED* = rgb[uint8](219, 112, 147)
  COLOR_PAPAYAWHIP* = rgb[uint8](255, 239, 213)
  COLOR_PEACHPUFF* = rgb[uint8](255, 218, 185)
  COLOR_PERU* = rgb[uint8](205, 133, 63)
  COLOR_PINK* = rgb[uint8](255, 192, 203)
  COLOR_PLUM* = rgb[uint8](221, 160, 221)
  COLOR_POWDERBLUE* = rgb[uint8](176, 224, 230)
  COLOR_PURPLE* = rgb[uint8](128, 0, 128)
  COLOR_RED* = rgb[uint8](255, 0, 0)
  COLOR_ROSYBROWN* = rgb[uint8](188, 143, 143)
  COLOR_ROYALBLUE* = rgb[uint8]( 65, 105, 225)
  COLOR_SADDLEBROWN* = rgb[uint8](139, 69, 19)
  COLOR_SALMON* = rgb[uint8](250, 128, 114)
  COLOR_SANDYBROWN* = rgb[uint8](244, 164, 96)
  COLOR_SEAGREEN* = rgb[uint8]( 46, 139, 87)
  COLOR_SEASHELL* = rgb[uint8](255, 245, 238)
  COLOR_SIENNA* = rgb[uint8](160, 82, 45)
  COLOR_SILVER* = rgb[uint8](192, 192, 192)
  COLOR_SKYBLUE* = rgb[uint8](135, 206, 235)
  COLOR_SLATEBLUE* = rgb[uint8](106, 90, 205)
  COLOR_SLATEGRAY* = rgb[uint8](112, 128, 144)
  COLOR_SLATEGREY* = rgb[uint8](112, 128, 144)
  COLOR_SNOW* = rgb[uint8](255, 250, 250)
  COLOR_SPRINGGREEN* = rgb[uint8]( 0, 255, 127)
  COLOR_STEELBLUE* = rgb[uint8]( 70, 130, 180)
  COLOR_TAN* = rgb[uint8](210, 180, 140)
  COLOR_TEAL* = rgb[uint8]( 0, 128, 128)
  COLOR_THISTLE* = rgb[uint8](216, 191, 216)
  COLOR_TOMATO* = rgb[uint8](255, 99, 71)
  COLOR_TURQUOISE* = rgb[uint8]( 64, 224, 208)
  COLOR_VIOLET* = rgb[uint8](238, 130, 238)
  COLOR_WHEAT* = rgb[uint8](245, 222, 179)
  COLOR_WHITE* = rgb[uint8](255, 255, 255)
  COLOR_WHITESMOKE* = rgb[uint8](245, 245, 245)
  COLOR_YELLOW* = rgb[uint8](255, 255, 0)
  COLOR_YELLOWGREEN* = rgb[uint8](154, 205, 50)
