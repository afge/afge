import nimterop/[build, cimport]

const
  STB_GIT_URL {.strdefine.} = "https://github.com/nothings/stb"
  baseDir = getProjectCacheDir("afge" / "stb")

static:
  gitPull(STB_GIT_URL, baseDir)
  writeFile(baseDir / "stb_image.c", "#define STB_IMAGE_IMPLEMENTATION\n#include \"stb_image.h\"")

cCompile(findFile("stb_image.c", baseDir))
cImport(findFile("stb_image.h", baseDir))

when isMainModule:
  echo STBI_VERSION
