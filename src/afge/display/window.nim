import ../wrapper/[glfw, gl]
import ../utils/[span, macroutils]

import options
import macros

export glfw
export span

type 
  Window* = ptr GLFWwindow
  Monitor* = ptr GLFWmonitor
  VideoMode* = ptr GLFWvidmode
  # may be moved to utils package(/dir/etc.)
  ImplicitOption*[T] = distinct Option[T]

proc isSome[T](opt: ImplicitOption[T]): bool =
  return (Option[T] opt).isSome
proc value[T](opt: ImplicitOption[T]): T =
  return (Option[T] opt).get()

template nativeHint(hint: int) {.pragma.}

macro generateWindowInitData(objdecl: untyped): untyped =
  result = newStmtList()

  type Hint = tuple[name, glfwHint: NimNode]
  var hints = newSeq[Hint]()

  let fields = objdecl[0][0][2][2]

  for field in fields:
    # wrap in ImplicitOption
    field[1] = nnkBracketExpr.newTree(bindSym"ImplicitOption", field[1])

    var idx = -1
    var name = getMainIdent(field[0])
    var glfwHint = ident("GLFW" & $name) # visible -> GLFWvisible == GLFW_VISIBLE
    
    for pragma in field[0][1]:
      case pragma.kind:
      of nnkExprColonExpr, nnkCall:
        if $pragma[0] == "nativeHint":
          glfwHint = pragma[1]
          assert pragma.len == 2
      else:
        continue

    # delete the pragma (optional)
    if idx >= 0:
      field[0][1].del(idx) 

    hints.add (name: name, glfwHint: glfwHint)

  var hintsVar = ident("hints")
  var applyWindowHints = quote do:
    {.push warning[Deprecated]: off.}
    proc applyWindowHints(`hintsVar`: WindowInitData) =
      glfwDefaultWindowHints()
    {.pop.}

  for (name, glfwHint) in hints:
    applyWindowHints[1][6].add quote do:
      if `hintsVar`.`name`.isSome:
        setWindowHint(`glfwHint`, `hintsVar`.`name`.value)
  let typeSection = objdecl[0]
  result.add typeSection
  result.add applyWindowHints

template setWindowHint(hint: int, value: typed) =
  when typeof(value) is string:
    glfwWindowHintString(cint hint, cstring value)
  else:
    glfwWindowHint(cint hint, cint value)

generateWindowInitData:
  type
    WindowInitData* = object
      resizable*: bool
      visible*: bool
      decorated*: bool
      focused*: bool
      autoIconify*: bool
      floating*: bool
      maximized*: bool
      centerCursor*: bool
      transparentFramebuffer*: bool
      focusOnShow*: bool
      scaleToMonitor*: bool
      redBits*: int
      greenBits*: int
      blueBits*: int
      alphaBits*: int
      depthBits*: int
      stencilBits*: int
      accumRedBits*: int
      accumGreenBits*: int
      accumBlueBits*: int
      accumAlphaBits*: int
      auxBuffers*: int
      samples*: int
      refreshRate*: int
      stereo*: bool
      srgbCapable*: bool
      doublebuffer*: bool
      clientApi*: cint
      contextCreationApi*: cint
      contextVersionMajor*: int
      contextVersionMinor*: int
      contextRobustness*: cint
      contextReleaseBehavior*: cint
      openglForwardCompat*: bool
      openglDebugContext* {.nativeHint: GLFW_CONTEXT_DEBUG.}: bool
      openglProfile*: cint
      cocoaRetinaFramebuffer*: bool
      cocoaFrameName*: string
      cocoaGraphicsSwitching*: bool
      x11ClassName*: string
      x11InstanceName*: string

converter toOption*[T](value: T): ImplicitOption[T] = 
  result = ImplicitOption[T] some(value)

template initWindowSystem*() =
  if glfwInit() == GLFW_FALSE:
    raise OSError.newException("unable to init window system")

template terminateWindowSystem*() =
  glfwTerminate()

template getPrimaryMonitor*(): Monitor = glfwGetPrimaryMonitor()

template getMonitors*(): Span[Monitor] =
  var len: cint
  let data = glfwGetMonitors(addr len)
  newSpan(data, len)

template getMonitorName*(monitor: Monitor): string =
  $glfwGetMonitorName(monitor)

template createWindow*(title: string, width: int, height: int, hints = default(WindowInitData), monitor = Monitor nil, share = Window nil): Window =
  applyWindowHints(hints)
  glfwCreateWindow(cint width, cint height, cstring title, monitor, share)

template createFullscreenWindow*(title: string, width = -1, height = -1, monitor = getPrimaryMonitor(), hints = default(WindowInitData)): Window =
  let videoMode = monitor.glfwGetVideoMode()
  applyWindowHints(hints)
  glfwCreateWindow(
    if width < 0: videoMode.width else: cint(width),
    if height < 0: videoMode.height else: cint(height),
    title, monitor, nil
  )

template getFramebufferSize*(window: Window): (cint, cint) =
  var
    w, h: cint
  glfwGetFramebufferSize(window, addr w, addr h)
  (w, h)

template makeContextCurrent*(window: Window) =
  glfwMakeContextCurrent(window)

template shouldClose*(window: Window): bool = 
  glfwWindowShouldClose(window) == GLFW_TRUE

template pollWindowSystemEvents*() =
  glfwPollEvents()

template swapBuffers*(window: Window) =
  glfwSwapBuffers(window)

template destroy*(window: Window) =
  glfwDestroyWindow(window)

template loadGLFunctions*() =
  if not gladLoadGL(glfwGetProcAddress):
    raise OSError.newException("unable to load OpenGL functions")

template getKey*(window: Window, key: cint): bool =
  glfwGetKey(window, cint key) == GLFW_PRESS

template getMouseButton*(window: Window, mbutton: cint): bool =
  glfwGetMouseButton(window, cint mbutton) == GLFW_PRESS

template getCursorPosition*(window: Window): (cdouble, cdouble) =
  var x, y: cdouble
  glfwGetCursorPos(window, addr x, addr y)
  (x, y)
