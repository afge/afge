import macros
import ../utils/macroutils
import tables

type DynamicObject* = object

# make dynamic aligned structs
macro dynamic*(typedecls: untyped) =
  result = newStmtList()
  for typedecl in typedecls[0]:
    let typename = $typedecl[0].getMainIdent()
    let exported = typedecl.isExportDecl()
    let typenameNode = createIdentifier(typename, exported)
    let unexportTypename = createIdentifier(typename, false)
    let sizeVarNameNode = createIdentifier("sizeof" & typename, false)
    let alignVarNameNode = createIdentifier("align" & typename, false)
    let dynSizeofNameNode = createIdentifier("dynSizeof", exported)
    let dynAlignofNameNode = createIdentifier("dynAlignof", exported)
    result.add quote do:
      type
        `typenameNode` = distinct DynamicObject
      var
        `sizeVarNameNode` = 0
        `alignVarNameNode` = 0
      proc `dynSizeofNameNode`[T: `unexportTypename`](t: typedesc[T]): ptr int = addr `sizeVarNameNode`
      proc `dynAlignofNameNode`[T: `unexportTypename`](t: typedesc[T]): ptr int = addr `alignVarNameNode`
    
    let calcSizeAlignofProcNameNode = createIdentifier("calcSizeAlignof", exported)
    let unexportedCalcSizeAlignofProcNameNode = createIdentifier("calcSizeAlignof", false)
    let calcSizeAlignofProc = quote do:
      proc `calcSizeAlignofProcNameNode`[T: `unexportTypename`](t: typedesc[T]) =
        `sizeVarNameNode` = 0
        `alignVarNameNode` = 0
      `unexportedCalcSizeAlignofProcNameNode`(`unexportTypename`)

    let allocProcNameNode = createIdentifier("alloc" & $unexportTypename, exported)
    let callocProcNameNode = createIdentifier("alloc0" & $unexportTypename, exported)
    let deallocProcNameNode = createIdentifier("dealloc" & $unexportTypename, exported)
    result.add quote do:
      proc `allocProcNameNode`(): ptr `unexportTypename` =
        return cast[ptr `unexportTypename`](alloc(`sizeVarNameNode`))
      proc `callocProcNameNode`(): ptr `unexportTypename` =
        return cast[ptr `unexportTypename`](alloc0(`sizeVarNameNode`))
      proc `deallocProcNameNode`(obj: ptr `unexportTypename`) =
        dealloc(pointer obj)

    let obj = ident("obj")
    let callback = ident("callback")
    var fieldIterateTemplate = quote do:
      template iterateFields*(`obj`: ptr `unexportTypename`, `callback`: untyped) =
        discard
    fieldIterateTemplate[6].del 0

    for fieldNode in typedecl[2][2]:
      var fieldNames = newSeq[NimNode]()
      if fieldNode[0].kind == nnkIdentDefs:
        for i in 0 ..< fieldNode[0].len - 2:
          fieldNames.add fieldNode[0][i]
      else:
        fieldNames.add fieldNode[0]

      let typ = fieldNode[^2]
      for fieldName in fieldNames:
        let nameNode = fieldName.getMainIdent()

        let offsetNameNode = ident(typename & "offset" & $nameNode)
        let alignNameNode = ident(typename & "align" & $nameNode)
        let setterNameNode = nnkAccQuoted.newTree(nameNode, ident("="))
        let offsetProcNameNode = ident("dynOffset" & $nameNode)
        let alignProcNameNode = ident("dynAlign" & $nameNode)

        let offsetAlignVar = quote do:
          var `offsetNameNode` = 0
          var `alignNameNode` = 0
        let getterProc = quote do:
          proc `nameNode`(obj: ptr `unexportTypename`): ptr `typ` =
            return cast[ptr `typ`](cast[ByteAddress](obj) + `offsetNameNode`)
        let setterProc = quote do:
          proc `setterNameNode`*(obj: ptr `unexportTypename`, value: `typ`) =
            cast[ptr `typ`](cast[ByteAddress](obj) + `offsetNameNode`)[] = value
        let offsetProc = quote do:
          proc `offsetProcNameNode`*[T: `unexportTypename`](t: typedesc[T]): ptr int =
            return addr `offsetNameNode`
        let alignProc = quote do:
          proc `alignProcNameNode`*[T: `unexportTypename`](t: typedesc[T]): ptr int =
            return addr `alignNameNode`

        if fieldNode[0].isExportDecl():
          getterProc.setExportDecl(true)
          setterproc.setExportDecl(true)
          offsetProc.setExportDecl(true)
          alignProc.setExportDecl(true)

        # let setterProc = quote do:
        result.add offsetAlignVar
        result.add getterProc
        result.add setterProc
        result.add offsetProc
        result.add alignProc

        calcSizeAlignofProc[0][6].add quote do:
          if `sizeVarNameNode` < `offsetNameNode` + sizeof `typ`:
            `sizeVarNameNode` = `offsetNameNode` + sizeof `typ`
          if `alignVarNameNode` < `alignNameNode`:
            `alignVarNameNode` = `alignNameNode`

        let fieldNameLit = newLit($nameNode)
        fieldIterateTemplate[6].add quote do:
          `callback`(`fieldNameLit`, `obj`.`nameNode`[])
        
    result.add calcSizeAlignofProc
    result.add fieldIterateTemplate

macro offset*(typ, member: untyped): untyped =
  result = nnkBracketExpr.newTree(newCall("dynOffset" & $member, typ))

macro offsetStr*(typ: untyped, member: static[string]): untyped =
  result = nnkBracketExpr.newTree(newCall("dynOffset" & member, typ))

macro align*(typ, member: untyped): untyped =
  result = nnkBracketExpr.newTree(newCall("dynAlign" & $member, typ))

macro alignStr*(typ: untyped, member: static[string]): untyped =
  result = nnkBracketExpr.newTree(newCall("dynAlign" & member, typ))