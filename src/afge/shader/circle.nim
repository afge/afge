import os
import macros
import nimterop/build
import strformat

import ../graphics/glw

# only supported on linux (blame the circle dude not me)
# this need cmake
const 
  baseDir = getProjectCacheDir("afge")
when not defined(AFGE_GLOBAL_SPIRV_CROSS):
  const
    SPIRV_CROSS_GIT_URL {.strdefine.} = "https://github.com/KhronosGroup/SPIRV-Cross"
    spirvCrossDir = baseDir / "SPIRV-Cross"
    spirvCrossBuildDir = spirvCrossDir / "build"
    spirvCrossExec = spirvCrossBuildDir / "spirv-cross"
  
  static:
    if not spirvCrossDir.dirExists():
      echo fmt"spirv-cross not found. Building from source:  {SPIRV_CROSS_GIT_URL}"
      gitPull(SPIRV_CROSS_GIT_URL, spirvCrossDir)
      assert execAction(fmt"cmake -B{spirvCrossBuildDir} {spirvCrossDir}").ret == 0
      assert execAction(fmt"cmake --build {spirvCrossBuildDir}").ret == 0
else:
  const
    AFGE_GLOBAL_SPIRV_CROSS {.strdefine.} = ""
    spirvCrossExec = AFGE_GLOBAL_SPIRV_CROSS

when not defined(AFGE_GLOBAL_CIRCLE):
  const
    CIRCLE_RELEASE_URL {.strdefine.} = "https://circle-lang.org/linux/build_131.tgz"
    circleExec = baseDir / "circle"
  static:
    if not circleExec.fileExists():
      echo fmt"circle not found. Downloading lastest release"
      downloadUrl(CIRCLE_RELEASE_URL, baseDir)
else:
  const 
    AFGE_GLOBAL_CIRCLE {.strdefine.} = ""
    circleExec = AFGE_GLOBAL_CIRCLE

proc circleFileToGLSL(filename: string, stage: string): string =
  when defined(nimsuggest) or defined(nimcheck):
    return ""
  else:
    discard slurp(filename.absolutePath(getProjectDir()))
    let baseDirSanitized = baseDir.sanitizePath
    var ext = "glsl"
    if stage != "":
      ext = stage
    let outputFile = (baseDir / ("shader_" & $hash(filename) & "." & ext)).sanitizePath()
    let cache = $hash(readFile(filename.absolutePath(getProjectDir())))
    
    var stageFlags = ""
    if stage != "":
      stageFlags = fmt"--stage {stage}"
    exec(fmt"{circleExec.sanitizePath} -shader {filename.absolutePath(getProjectDir()).sanitizePath} -o {baseDirSanitized}/tempshader.spv", cache)
    exec(fmt"{spirvCrossExec.sanitizePath} --version 330 --no-es --separate-shader-objects {baseDirSanitized}/tempshader.spv {stageFlags} --output {outputFile}", cache)
    return readFile(outputFile)

proc circleCodeToGLSL(code: string, stage: string): string =
  writeFile(fmt"{baseDir}/tempshader.cxx", code)
  return circleFileToGLSL(fmt"{baseDir}/tempshader.cxx", stage)

proc toSPIRVShaderType(typ: string): string =
  return case typ:
  of "VERTEX": "vert"
  of "FRAGMENT": "frag"
  else: ""

macro circle*(code: static[string], stages: openArray[ShaderType] = []): untyped =
  if stages.len == 0:
    # first entry point in shader
    result = newLit(circleCodeToGLSL(code, ""))
  result = nnkBracket.newTree()
  for stage in stages:
    let shaderSource = newLit(circleCodeToGLSL(code, toSPIRVShaderType($stage)))
    result.add quote do:
      (`stage`, `shaderSource`)

macro circleFile*(path: static[string], stages: openArray[ShaderType] = []): untyped =
  if stages.len == 0:
    # first entry point in shader
    result = newLit(circleFileToGLSL(path, ""))
  result = nnkBracket.newTree()
  for stage in stages:
    let shaderSource = newLit(circleFileToGLSL(path, toSPIRVShaderType($stage)))
    result.add quote do:
      (`stage`, `shaderSource`)

when isMainModule:
  for (kind, source) in circle("""
  # sample code from https://github.com/seanbaxter/shaders
  #include <stdint.h>
  [[spirv::in(0)]]
  vec3 in_position_vs;

  [[spirv::in(1)]]
  vec2 in_texcoord_vs;

  [[spirv::out(1)]]
  vec2 out_texcoord_vs;


  [[spirv::in(1)]]
  vec2 in_texcoord_fs;

  [[spirv::out(0)]]
  vec4 out_color_fs;

  [[spirv::uniform(0)]]
  sampler2D texture_sampler;

  struct uniforms_t {
    mat4 view_proj;
    float seconds;
  };

  [[spirv::uniform(0)]]
  uniforms_t uniforms;

  [[spirv::vert]]
  void vert_main() {
    // Create a rotation matrix.
    // mat4 rotate = make_rotateY(uniforms.seconds);
    mat4 rotate = mat4(1.0);

    // Rotate the position.
    vec4 position = rotate * vec4(in_position_vs, 1);

    // Write to a builtin variable.
    glvert_Output.Position = uniforms.view_proj * position;

    // Pass texcoord through.
    out_texcoord_vs = in_texcoord_vs;
  }

  [[spirv::frag]]
  void frag_main() {
    // Load the inputs. The locations correspond to the outputs from the
    // vertex shader.
    vec2 texcoord = in_texcoord_fs;
    vec4 color = texture(texture_sampler, texcoord);
    color.a = int16_t(texcoord.x);

    // Write to a variable template.
    out_color_fs = color;
  }
  """, [ShaderType.VERTEX, ShaderType.FRAGMENT]):
    echo kind
    echo source



  
