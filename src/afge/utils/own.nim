import options
type
  Own*[T] = object
    value*: T
    own*: bool

template own*[T](v: T): Own[T] = Own[T](value: v, own: true)

template notOwn*[T](v: T): Own[T] = Own[T](value: v, own: false)

template ifOwn*[T, R](o: Own[T], callback: proc(v: T): R): Option[R] =
  result = if o.own: some(callback(o.value)) else: none()

template ifOwnVoid*[T](o: Own[T], callback: proc(v: T): void) =
  if o.own:
    callback(o.value)
