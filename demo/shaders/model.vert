#version 430

// better GLSL linting coming soon
#ifdef GLSLLINT
#define BONE_MATRICES_COUNT 727
#extension GL_GOOGLE_include_directive: require
#define AFGE_ApplyBoneAnimation(a0, a1, a2, a3, a4, a5, a6)
#else
#include "animation.glslib"
#endif

layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 tcoords;
layout (location = 2) in vec4 boneWeight;
layout (location = 3) in ivec4 boneIds;

layout (location = 0) out vec2 vf_tcoords;

layout (std140, binding = 1) uniform MatrixSuite {
  mat4 proj;
  mat4 view;
  mat4 trans;
  mat4 boneTransforms[BONE_MATRICES_COUNT];
};                                 

void main() {
  vec4 totalPos = vec4(0.0);
  AFGE_ApplyBoneAnimation(totalPos, pos, 4, boneTransforms,
                          boneIds, boneWeight, BONE_MATRICES_COUNT);
  gl_Position = proj * view * trans * totalPos;
  vf_tcoords = tcoords;
}