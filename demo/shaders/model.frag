#version 430
layout (location = 0) in vec2 vf_tcoords;
layout (location = 0) out vec4 color;

layout (binding = 0) uniform sampler2D diffuse;

void main() {
  color = texture(diffuse, vf_tcoords);
}