import macros

proc panic() {.noreturn.} =
  error "unsupported behavior"

proc sizeofTypedecl*(typedecl: NimNode): NimNode =
  expectKind typedecl, nnkTypeSection
  for field in typedecl[0][2][2].children:
    let sizeOfThis = newCall("sizeof", field[1])
    if isNil(result):
      result = sizeOfThis
    else:
      result = infix(result, "+", sizeOfThis)

proc getMainIdent*(identBasedNode: NimNode): NimNode =
  case identBasedNode.kind:
  of nnkIdent: return identBasedNode
  of nnkPostfix: return identBasedNode[1].getMainIdent()
  of nnkPragmaExpr: return identBasedNode[0].getMainIdent()
  else: 
    echo identBasedNode.treeRepr
    panic()

proc getTypeName*(typedecl: NimNode): tuple[name: string, exported: bool] =
  let typenameNode = typedecl[0]
  case typenameNode.kind:
  of nnkIdent:
    result = (name: $typenameNode, exported: false)
  of nnkPostfix:
    for node in typenameNode:
      if node.kind == nnkIdent:
        if $node == "*":
          result.exported = true
        else:
          result.name = result.name & $node
  else:
    echo typedecl.treeRepr
    panic()

proc createIdentifier*(value: string, exported: bool): NimNode =
  result = ident value
  if exported:
    result = nnkPostfix.newTree(ident"*", result)

proc isExportDecl*(node: NimNode): bool =
  case node.kind:
  of nnkPostfix:
    return node.kind == nnkPostfix and node[0].kind == nnkIdent and $node[0] == "*"
  of nnkProcDef, nnkTypeDef, nnkIdentDefs, nnkPragmaExpr:
    return node[0].isExportDecl()
  of nnkIdent, nnkAccQuoted:
    return false
  else:
    echo node.treeRepr
    panic()

proc setExportDecl*(node: NimNode, exported: bool) =
  case node.kind:
  of nnkProcDef:
    if node.isExportDecl == exported:
      return

    if exported:
      node[0] = nnkPostfix.newTree(ident("*"), node[0])
    else:
      node[0] = node[0][1]
  else:
    panic()

proc visitChildren*(node: var NimNode, kinds: NimNodeKinds = {}, f: proc(n: var NimNode)) =
  # empty NimNodeKinds set will match all nodes, use {nnkNone} to exclude everything
  if node.kind in kinds or kinds.len == 0:
    f(node)
  for i in 0 ..< node.len:
    var child = node[i]
    visitChildren(child, kinds, f)
    node[i] = child
