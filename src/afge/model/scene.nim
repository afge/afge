import tables
import macros
import times

import glm

import ../wrapper/assimp
import ../graphics/[glw, mesh]
import ../utils/[span, own]

import animation
type
  Scene* = ptr aiScene
  SceneNode* = object
    scene*: Scene
    data*: ptr aiNode

  SceneLoadingOptions {.pure, size: 4.} = enum
    CALC_TANGENT_SPACE = aiProcess_CalcTangentSpace
    JOIN_IDENTICAL_VERTICES = aiProcess_JoinIdenticalVertices
    MAKE_LEFT_HANDED = aiProcess_MakeLeftHanded
    TRIANGULATE = aiProcess_Triangulate
    REMOVE_COMPONENT = aiProcess_RemoveComponent
    GENERATE_NORMALS = aiProcess_GenNormals
    GENERATE_SMOOTH_NORMALS = aiProcess_GenSmoothNormals
    SPLIT_LARGE_MESHES = aiProcess_SplitLargeMeshes
    PRETRANSFORM_VERTICES = aiProcess_PreTransformVertices
    LIMIT_BONE_WEIGHTS = aiProcess_LimitBoneWeights
    VALIDATE_DATA_STRUCTURE = aiProcess_ValidateDataStructure
    IMPROVE_CACHE_LOCALITY = aiProcess_ImproveCacheLocality
    REMOVE_REDUNDANT_MATERIALS = aiProcess_RemoveRedundantMaterials
    FIX_INFACING_NORMALS = aiProcess_FixInfacingNormals
    POPULATE_ARMATURE_DATA = aiProcess_PopulateArmatureData
    SORT_BY_PTYPE = aiProcess_SortByPType
    FIND_DEGENERATES = aiProcess_FindDegenerates
    FIND_INVALID_DATA = aiProcess_FindInvalidData
    GEN_UV_COORDS = aiProcess_GenUVCoords
    TRANSFORM_UV_COORDS = aiProcess_TransformUVCoords
    FIND_INSTANCES = aiProcess_FindInstances
    OPTIMIZE_MESHES = aiProcess_OptimizeMeshes
    OPTIMIZE_GRAPH = aiProcess_OptimizeGraph
    FLIP_UVS = aiProcess_FlipUVs
    FLIP_WINDING_ORDER = aiProcess_FlipWindingOrder
    SPLIT_BY_BONE_COUNT = aiProcess_SplitByBoneCount
    DEBONE = aiProcess_Debone
    GLOBAL_SCALE = aiProcess_GlobalScale
    EMBED_TEXTURES = aiProcess_EmbedTextures
    FORCE_GENERATE_NORMALS = aiProcess_ForceGenNormals
    DROP_NORMALS = aiProcess_DropNormals
    GENERATE_BOUNDING_BOXES = 1 shl 31  # aiProcess_GenBoundingBoxes

  SceneLoadingOptionsBitfield* = distinct int32

  SceneMeshAttribute* = object
    typ*: SceneMeshAttributeType
    arg*: int16

  SceneMeshAttributeType* = enum
    POSITION
    TEXCOORDS
    NORMALS
    TANGENTS
    BITANGENTS
    COLORS
    BONE_DATA 

  SceneID*[T] = distinct cuint
  RawSceneMesh* = ptr aiMesh
  SceneMaterial* = ptr aiMaterial
  RawSceneAnimation* = ptr aiAnimation
  SceneTexture* = ptr aiTexture
  SceneLight* = ptr aiLight
  SceneCamera* = ptr aiCamera

converter toBitfield*(opt: SceneLoadingOptions): auto = SceneLoadingOptionsBitfield opt.ord
converter toAttrib*(typ: SceneMeshAttributeType): auto = 
  let defaultArg = case typ:
    of SceneMeshAttributeType.POSITION, SceneMeshAttributeType.NORMALS,
      SceneMeshAttributeType.TANGENTS, SceneMeshAttributeType.BITANGENTS: 3i16
    of SceneMeshAttributeType.BONE_DATA: 4i16
    else: 0i16
  return SceneMeshAttribute(typ: typ, arg: defaultArg)

# `and` makes more sense than `or`
proc `and`*(x, y: SceneLoadingOptionsBitfield): auto =
  return SceneLoadingOptionsBitfield(x.int32 or y.int32)

const slobPresetRealTimeFast* = 
  SceneLoadingOptions.CALC_TANGENT_SPACE and
  SceneLoadingOptions.GENERATE_NORMALS and
  SceneLoadingOptions.JOIN_IDENTICAL_VERTICES and
  SceneLoadingOptions.TRIANGULATE and
  SceneLoadingOptions.GEN_UV_COORDS and
  SceneLoadingOptions.SORT_BY_PTYPE

const slobPresetRealTimeQuality* =
  SceneLoadingOptions.CALC_TANGENT_SPACE and
  SceneLoadingOptions.GENERATE_NORMALS and
  SceneLoadingOptions.JOIN_IDENTICAL_VERTICES and
  SceneLoadingOptions.IMPROVE_CACHE_LOCALITY and
  SceneLoadingOptions.LIMIT_BONE_WEIGHTS and
  SceneLoadingOptions.REMOVE_REDUNDANT_MATERIALS and
  SceneLoadingOptions.SPLIT_LARGE_MESHES and
  SceneLoadingOptions.TRIANGULATE and
  SceneLoadingOptions.GEN_UV_COORDS and
  SceneLoadingOptions.SORT_BY_PTYPE and
  SceneLoadingOptions.FIND_DEGENERATES and
  SceneLoadingOptions.FIND_INVALID_DATA

const slobPresetRealTimeMaxQuality* =
  slobPresetRealTimeQuality and
  SceneLoadingOptions.FIND_INSTANCES and
  SceneLoadingOptions.VALIDATE_DATA_STRUCTURE and
  SceneLoadingOptions.OPTIMIZE_MESHES

using scene: Scene

proc loadScene*(file: string, loadOptions: SceneLoadingOptionsBitfield = slobPresetRealTimeFast): Scene =
  return aiImportFile(cstring file, cuint loadOptions)

proc destroy*(scene: Scene) =
  aiReleaseImport(scene)

proc rootNode*(scene): SceneNode = SceneNode(scene: scene, data: scene.mRootNode)
proc meshes*(scene): Span[RawSceneMesh] = newSpan(scene.mMeshes, int scene.mNumMeshes)
proc materials*(scene): Span[SceneMaterial] = newSpan(scene.mMaterials, int scene.mNumMaterials)
proc animations*(scene): Span[RawSceneAnimation] = newSpan(scene.mAnimations, int scene.mNumAnimations)
proc textures*(scene): Span[SceneTexture] = newSpan(scene.mTextures, int scene.mNumTextures)
proc lights*(scene): Span[SceneLight] = newSpan(scene.mLights, int scene.mNumLights)
proc cameras*(scene): Span[SceneCamera] = newSpan(scene.mCameras, int scene.mNumCameras)
proc `[]`*[C, T](arr: C, id: SceneID[T]): T = arr[int id]

# the functions below do the same thing, but different names to make the code easier to read

# typ = COLORS or TEXCOORDS, specify the index of the texcoords/colors set
# default value is 0
proc index*(typ: SceneMeshAttributeType, idx: int): auto  = SceneMeshAttribute(typ: typ, arg: int16 idx)

# typ = BONE_DATA, specify the maximum number of bone weights per vertex
# if max = 4k + r (r is 0, 1, 2 or 3), the generated VAO will have (k + 1) attributes for bone weights (or k if r == 0)
# the first k attributes will be vec4 of floats, and the last one (if r != 0) will be a vec`r`.
# the same thing for bone IDs, k ivec4 and 1 (if r != 0) ivec`r` for the ID
# Note: the ID will be stored after the weights, so your shader should have something like: (max = 6)
#   layout(location = 2) in vec4 boneWeights1;
#   layout(location = 3) in vec2 boneWeights2;
#   layout(location = 4) in vec4 boneIds1;
#   layout(location = 5) in vec2 boneIds2;
# to save attributes, max should be a multiple of 4, default value is 4
proc maxBones*(typ: SceneMeshAttributeType, max: int): auto = index(typ, max)

# typ = POSITION, NORMALS, TANGENTS or BITANGENTS, specify the dimension of the model
# default value is 3
proc dimensions*(typ: SceneMeshAttributeType, dim: int): auto = index(typ, dim)

type ObjectOrPtr[T] = T | ptr T
converter toVec2*(v: ObjectOrPtr[aiVector2D]): Vec2[ai_real] =
  return vec2(v.x, v.y)
converter toVec3*(v: ObjectOrPtr[aiVector3D]): Vec3[ai_real] =
  return vec3(v.x, v.y, v.z)
converter toVec3*(v: ObjectOrPtr[aiColor3D]): Vec3[ai_real] =
  return vec3(v.x, v.y, v.z)
converter toVec4*(v: ObjectOrPtr[aiColor4D]): Vec4[ai_real] =
  return vec4(v.r, v.g, v.b, v.a)
converter toMat3*(m: ObjectOrPtr[aiMatrix3x3]): Mat3[ai_real] =
  return mat3(vec3(m.a1, m.b1, m.c1),
              vec3(m.a2, m.b2, m.c2),
              vec3(m.a3, m.b3, m.c3))
converter toMat4*(m: ObjectOrPtr[aiMatrix4x4]): Mat4[ai_real] =
  return mat4(vec4(m.a1, m.b1, m.c1, m.d1),
              vec4(m.a2, m.b2, m.c2, m.d2),
              vec4(m.a3, m.b3, m.c3, m.d3),
              vec4(m.a4, m.b4, m.c4, m.d4))
converter toQuat*(q: ObjectOrPtr[aiQuaternion]): Quat[ai_real] =
  return quat(q.x, q.y, q.z, q.w)

proc positions*(rawMesh: RawSceneMesh): auto = newSpan(rawMesh.mVertices, int rawMesh.mNumVertices)
proc textureCoords*(rawMesh: RawSceneMesh): auto = newSpan(rawMesh.mVertices, int rawMesh.mNumVertices)
proc normals*(rawMesh: RawSceneMesh): auto = newSpan(rawMesh.mVertices, int rawMesh.mNumVertices)
proc tangents*(rawMesh: RawSceneMesh): auto = newSpan(rawMesh.mVertices, int rawMesh.mNumVertices)
proc bitangents*(rawMesh: RawSceneMesh): auto = newSpan(rawMesh.mVertices, int rawMesh.mNumVertices)
proc mColors*(rawMesh: RawSceneMesh): auto = newSpan(rawMesh.mVertices, int rawMesh.mNumVertices)

proc bakeAnimation*(rawAnim: RawSceneAnimation, scene: Scene, bones: var Table[string, Bone]): MeshAnimation =
  result.duration = rawAnim.mDuration
  result.ticksPerSecond = rawAnim.mTicksPerSecond

  proc convertNode(node: ptr aiNode): MeshAnimationNode =
    result.name = $cstring(addr node.mName.data[0])
    result.transform = node.mTransformation
    let children = newSpan(node.mChildren, int node.mNumChildren)
    for child in children:
      result.children.add(convertNode(child))
  result.rootNode = convertNode(scene.mRootNode)

  let channels = newSpan(rawAnim.mChannels, int rawAnim.mNumChannels)
  for channel in channels:
    let boneName = $cstring(addr channel.mNodeName.data[0])

    if not bones.hasKey(boneName):
      bones[boneName] = Bone(id: bones.len, transform: mat4f(1.0))

    var bone = AnimatedBone(name: boneName)
    bone.bone = Bone(id: bones[boneName].id, transform: mat4f(1.0))
    for keyPosition in newSpan(channel.mPositionKeys, int channel.mNumPositionKeys):
      bone.keyPositions.add KeyPosition(timestamp: float keyPosition.mTime, data: keyPosition.mValue)
    for keyRotation in newSpan(channel.mRotationKeys, int channel.mNumRotationKeys):
      bone.keyRotations.add KeyRotation(timestamp: float keyRotation.mTime, data: keyRotation.mValue)
    for keyScale in newSpan(channel.mScalingKeys, int channel.mNumScalingKeys):
      bone.keyScales.add KeyScale(timestamp: float keyScale.mTime, data: keyScale.mValue)

    result.animBones.add bone
  result.bones = bones

# returns a table of bones, which can be passed to bakeAnimation to bake the animation only for this mesh
proc bakeMesh*(rawMesh: RawSceneMesh, attribs: openArray[SceneMeshAttribute], mesh: var Mesh,
               vbo, ebo = notOwn(default(Buffer))): BoneTable =
  var attribBuffer = vbo
  if vbo.value.GLuint == 0:
    attribBuffer = own(createGL(Buffer))
    mesh.addAttribBuffer(attribBuffer.value)
  
  var attribOffsets = newSeq[int](attribs.len + 1)  # last entry is the size of a vertex
  attribOffsets[0] = 0

  var boneDataAttribs = newSeq[tuple[offset, maxBones: int]](0) 
  # should have 0-1 item(s) if you don't want to waste your attributes
  for i, attrib in attribs.pairs:
    let size = case attrib.typ:
      of SceneMeshAttributeType.TEXCOORDS: int rawMesh.mNumUVComponents[attrib.arg]
      of SceneMeshAttributeType.COLORS: 4
      of SceneMeshAttributeType.BONE_DATA: 
        boneDataAttribs.add (offset: attribOffsets[i], maxBones: int attrib.arg)
        2 * attrib.arg # one for weight, one for id
      else: int attrib.arg  # all the other attributes conveniently use arg as their size
                            # for POSITION, NORMALS, TANGENTS and BITANGENTS, it's the dimension
                            # for BONE_WEIGHT and BONE_ID, it's the maximum number of bones per vertex
    attribOffsets[i + 1] = attribOffsets[i] + size
  
  var data = newSeq[float32](attribOffsets[^1] * int rawMesh.mNumVertices)
  var dataInt = newSpan(cast[ptr int32](addr data[0]), data.len) # to store bone
  var dataPtr = 0

  template putData(vec: Vec, maxDims: SomeInteger) =
    for i in 0 ..< min(int maxDims, len(vec.arr)):
      when vec[int i] is SomeInteger:
        dataInt[dataPtr] = int32 vec[int i]
      else:
        data[dataPtr] = float32 vec[int i]
      dataPtr.inc()

  template putData[T](arr: ptr T, idx: int, maxDims: SomeInteger) =
    putData(cast[ptr T](cast[ByteAddress](arr) + idx * sizeof T)[], maxDims)

  for vertexIdx in 0 ..< int rawMesh.mNumVertices:
    for attrib in attribs:
      case attrib.typ:
      of SceneMeshAttributeType.POSITION: putData(rawMesh.mVertices, vertexIdx, attrib.arg)
      of SceneMeshAttributeType.TEXCOORDS: putData(rawMesh.mTextureCoords[attrib.arg], vertexIdx, rawMesh.mNumUVComponents[attrib.arg])
      of SceneMeshAttributeType.NORMALS: putData(rawMesh.mNormals, vertexIdx, attrib.arg)
      of SceneMeshAttributeType.TANGENTS: putData(rawMesh.mTangents, vertexIdx, attrib.arg)
      of SceneMeshAttributeType.BITANGENTS: putData(rawMesh.mBitangents, vertexIdx, attrib.arg)
      of SceneMeshAttributeType.COLORS: putData(rawMesh.mColors[attrib.arg], vertexIdx, 4)
      of SceneMeshAttributeType.BONE_DATA:
        # use memset should maybe make things a little faster idk
        for j in 0 ..< attrib.arg:
          data[dataPtr] = 0.0
          dataPtr.inc()
        for j in 0 ..< attrib.arg:
          dataInt[dataPtr] = -1
          dataPtr.inc()

  for bone in newSpan(rawMesh.mBones, int rawMesh.mNumBones):
    let name = $cstring(addr bone.mName.data[0])
    if not result.hasKey(name):
      result[name] = Bone(id: result.len, transform: bone.mOffsetMatrix)
    let boneId = result[name].id
    for weight in newSpan(bone.mWeights, int bone.mNumWeights):
      let vertexId = int weight.mVertexId
      let weightValue = weight.mWeight
      for boneDataAttrib in boneDataAttribs:
        let boneWeightOffset = vertexId * attribOffsets[^1] + boneDataAttrib.offset
        let maxBones = boneDataAttrib.maxBones
        let boneIdOffset = boneWeightOffset + maxBones

        for i in 0 ..< maxBones:
          if dataInt[boneIdOffset + i] < 0:
            dataInt[boneIdOffset + i] = int32 boneId
            data[boneWeightOffset + i] = float32 weightValue
            break

  attribBuffer.value.setBufferData(data, BufferUsage.STATIC_DRAW)

  if dsaEnabled():
    mesh.vertexArray.value.bindVertexBufferDSA(attribBuffer.value, mesh.currentBinding, attribOffsets[^1] * sizeof float32, 0)
    for i in 0 ..< attribOffsets.len - 1:
      var offset = attribOffsets[i] * sizeof float32
      let size = attribOffsets[i + 1] - attribOffsets[i]
      if attribs[i].typ == SceneMeshAttributeType.BONE_DATA:
        let vec4Attribs = size div 4
        let remainder = size - 4 * vec4Attribs

        for attribType in [AttribType.ATTRIB_FLOAT, AttribType.ATTRIB_INT]:
          for i in 0 .. vec4Attribs:
            let attribSize = if i == vec4Attribs: remainder else: 4
            if attribSize == 0:
              break
            mesh.vertexArray.value.setVertexAttributeDSA(mesh.currentAttrib, mesh.currentBinding, cast[AttribSize](attribSize), attribType, offset)
            mesh.vertexArray.value.enableVertexAttribs(mesh.currentAttrib)
            mesh.currentAttrib.inc()
            offset += 4 * sizeof float32
      else:
        mesh.vertexArray.value.setVertexAttributeDSA(mesh.currentAttrib, mesh.currentBinding, cast[AttribSize](size), AttribType.ATTRIB_FLOAT, offset)
        mesh.vertexArray.value.enableVertexAttribs(mesh.currentAttrib)
        mesh.currentAttrib.inc()
    mesh.currentBinding.inc()
  else:
    for i in 0 ..< attribOffsets.len - 1:
      var offset = attribOffsets[i] * sizeof float32
      let size = attribOffsets[i + 1] - attribOffsets[i]
      if attribs[i].typ == SceneMeshAttributeType.BONE_DATA:
        let attribs = size div 2
        let vec4Attribs = attribs div 4
        let remainder = attribs - 4 * vec4Attribs

        for attribType in [AttribType.ATTRIB_FLOAT, AttribType.ATTRIB_INT]:
          for i in 0 .. vec4Attribs:
            let attribSize = if i == vec4Attribs: remainder else: 4
            if attribSize == 0:
              break
            mesh.vertexArray.value.setVertexAttribute(attribBuffer.value, mesh.currentAttrib, cast[AttribSize](attribSize), attribType, attribOffsets[^1] * 4, offset)
            mesh.vertexArray.value.enableVertexAttribs(mesh.currentAttrib)
            mesh.currentAttrib.inc()
            offset += 4 * sizeof float32
      else:
        mesh.vertexArray.value.setVertexAttribute(attribBuffer.value, mesh.currentAttrib, cast[AttribSize](size), AttribType.ATTRIB_FLOAT, attribOffsets[^1] * 4, offset)
        mesh.vertexArray.value.enableVertexAttribs(mesh.currentAttrib)
        mesh.currentAttrib.inc()

  var elementBuffer = ebo
  if ebo.value.GLuint == 0:
    elementBuffer = notOwn(createGL(Buffer))
    mesh.setElementBuffer(elementBuffer)

  let faces = newSpan(rawMesh.mFaces, int rawMesh.mNumFaces)
  var indicesData = newSeq[int32](rawMesh.mNumFaces * 3)
  var indicesPtr = 0
  for face in faces.items:
    assert face.mNumIndices == 3, "only triangle meshes are supported"
    let indices = newSpan(face.mIndices, 3)
    indicesData[indicesPtr] = int32 indices[0]
    indicesData[indicesPtr + 1] = int32 indices[1]
    indicesData[indicesPtr + 2] = int32 indices[2]
    indicesPtr += 3
  elementBuffer.value.setBufferData(indicesData, BufferUsage.STATIC_DRAW)
  mesh.elementBuffer.count = indicesData.len
  mesh.elementBuffer.elementType = ElementType.UINT
  
