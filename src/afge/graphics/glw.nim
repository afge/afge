import strformat
import macros
import options

import ../wrapper/gl
import ../utils/[span, wrapperutils]
import ../layout/layoututils
import color

export wrapperutils
export gl
export color

proc dsaEnabled*(): bool =
  GLAD_GL_ARB_direct_state_access and not defined(AFGE_DISABLE_OPENGL_DSA)

type
  VertexArray* = distinct GLuint
  Buffer* = distinct GLuint
  ShaderProgram* = distinct GLuint
  Shader* = distinct GLuint

  TextureTarget* {.pure.} = enum
    TEX_1D = GL_TEXTURE_1D,
    TEX_2D = GL_TEXTURE_2D,
    TEX_3D = GL_TEXTURE_3D

  Texture*[Target: static[TextureTarget]] = distinct GLuint
  Texture1D* = Texture[TEX_1D]
  Texture2D* = Texture[TEX_2D]
  Texture3D* = Texture[TEX_3D]

  AttribSize* = range[1..4]
  AttribDataType* = int8|int16|int32 | uint8|uint16|uint32 | float32|float64
  TypedBuffer*[T] = Buffer

  BufferUsage* {.pure.} = enum
    STREAM_DRAW = GL_STREAM_DRAW,
    STREAM_READ = GL_STREAM_READ,
    STREAM_COPY = GL_STREAM_COPY,
    STATIC_DRAW = GL_STATIC_DRAW,
    STATIC_READ = GL_STATIC_READ,
    STATIC_COPY = GL_STATIC_COPY,
    DYNAMIC_DRAW = GL_DYNAMIC_DRAW,
    DYNAMIC_READ = GL_DYNAMIC_READ, 
    DYNAMIC_COPY = GL_DYNAMIC_COPY
  AttribType* {.pure.} = enum
    ATTRIB_INT = cGL_INT
    ATTRIB_FLOAT = cGL_FLOAT
    ATTRIB_DOUBLE = cGL_DOUBLE
    ATTRIB_HALF_FLOAT = GL_HALF_FLOAT
    ATTRIB_FIXED = cGL_FIXED
  ElementType* {.pure.} = enum
    UBYTE = GL_UNSIGNED_BYTE
    USHORT = GL_UNSIGNED_SHORT
    UINT = GL_UNSIGNED_INT
  DrawType* {.pure.} = enum
    TRIANGLE = GL_TRIANGLES
  ShaderType* {.pure.} = enum
    FRAGMENT = GL_FRAGMENT_SHADER,
    VERTEX = GL_VERTEX_SHADER
  TextureInternalFormat* {.pure.} = enum
    RGBA = GL_RGBA
    RGB8 = GL_RGB8
    RGBA8 = GL_RGBA8
    R8 = GL_R8
    RG8 = GL_RG8
  TextureFormat* {.pure.} = enum
    DEPTH = GL_DEPTH_COMPONENT
    R = GL_RED
    RGB = GL_RGB
    RGBA = GL_RGBA
    BGR = GL_BGR
    BGRA = GL_BGRA
    RG = GL_RG
    DEPTH_STENCIL = GL_DEPTH_STENCIL
  TextureDataType* {.pure.} = enum
    UBYTE = GL_UNSIGNED_BYTE
    FLOAT = cGL_FLOAT
  ClearTarget* {.pure.} = enum
    DEPTH_BUFFER = GL_DEPTH_BUFFER_BIT
    STENCIL_BUFFER = GL_STENCIL_BUFFER_BIT
    COLOR_BUFFER = GL_COLOR_BUFFER_BIT
  ImmutableBufferUsage* {.pure.} = enum
    MAP_READ = GL_MAP_READ_BIT
    MAP_WRITE = GL_MAP_WRITE_BIT
    DYNAMIC_STORAGE = GL_DYNAMIC_STORAGE_BIT
    CLIENT_STORAGE = GL_CLIENT_STORAGE_BIT
  BufferMapAccess* {.pure.} = enum
    READ = GL_MAP_READ_BIT
    WRITE = GL_MAP_WRITE_BIT
  CullFace* {.pure.} = enum
    FRONT = GL_FRONT
    BACK = GL_BACK
    FRONT_AND_BACK = GL_FRONT_AND_BACK

proc arrAddr[T](arr: openArray[T]): ptr T =
  if arr.len == 0: nil else: unsafeAddr arr[0]
    
template bindVertexArray*(va: VertexArray) =
  glBindVertexArray(GLuint va)

proc texTarget[Target](t: typedesc[Texture[Target]]): TextureTarget =
  return Target

template bindTex*[Target](tex: Texture[Target], slot: int) =
  if dsaEnabled():
    glBindTextureUnit(GLuint slot, GLuint tex)
  else:
    glActiveTexture(GLenum GL_TEXTURE0 + GLenum slot)
    glBindTexture(GLenum Target, GLuint tex)

proc setDefaultTextureFilters*(tex: Texture) =
  if dsaEnabled():
    glTextureParameteri(GLuint tex, GL_TEXTURE_MIN_FILTER, GLint GL_LINEAR)
    glTextureParameteri(GLuint tex, GL_TEXTURE_MAG_FILTER, GLint GL_LINEAR)
    glTextureParameteri(GLuint tex, GL_TEXTURE_WRAP_S, GLint GL_REPEAT)
    glTextureParameteri(GLuint tex, GL_TEXTURE_WRAP_T, GLint GL_REPEAT)
  else:
    glBindTexture(GLenum Texture.Target, GLuint tex)
    glTexParameteri(GLenum Texture.Target, GL_TEXTURE_WRAP_S, GLint GL_REPEAT)
    glTexParameteri(GLenum Texture.Target, GL_TEXTURE_WRAP_T, GLint GL_REPEAT)
    glTexParameteri(GLenum Texture.Target, GL_TEXTURE_MIN_FILTER, GLint GL_LINEAR)
    glTexParameteri(GLenum Texture.Target, GL_TEXTURE_MAG_FILTER, GLint GL_LINEAR)

proc createGL*[T: VertexArray | Buffer | ShaderProgram | Texture](t: typedesc[T]): T {.inline.} =
  when T is VertexArray:
    var va: GLuint
    if dsaEnabled(): 
      glCreateVertexArrays(1, addr va) 
    else: 
      glGenVertexArrays(1, addr va)
    return VertexArray va
  elif T is Buffer:
    var b: GLuint
    if dsaEnabled(): 
      glCreateBuffers(1, addr b) 
    else: 
      glGenBuffers(1, addr b)
    return Buffer b
  elif T is ShaderProgram:
    return ShaderProgram glCreateProgram()
  elif T is Texture:
    var tex: GLuint
    if dsaEnabled():
      glCreateTextures(GLenum texTarget(T), 1, addr tex)
    else: 
      glGenTextures(1, addr tex)
    setDefaultTextureFilters(T tex)
    return T tex

proc createGL*[T: Shader](t: typedesc[T], shaderType: ShaderType): T {.inline.} =
  return Shader glCreateShader(GLenum shaderType)

template destroy*(va: VertexArray) =
  glDeleteVertexArrays(1, cast[ptr GLuint](unsafeAddr va))
template destroy*(b: Buffer) =
  glDeleteBuffers(1, cast[ptr GLuint](unsafeAddr b))
template destroy*(sp: ShaderProgram) =
  glDeleteProgram(GLuint sp)
template destroy*(s: Shader) =
  glDeleteShader(GLuint s)
template destroy*(t: Texture) =
  glDeleteTextures(1, cast[ptr GLuint](unsafeAddr t))

# note: 
# only the size of the buffer is immutable (you can't resize the buffer)
# if you pass GL_MAP_WRITE_BIT (ImmutableBufferUsage.MAP_WRITE), you can still write to the buffer
# (there are still other ways to write to the buffer btw)
template allocDataImmutable*(buffer: Buffer, size: int, usage: set[ImmutableBufferUsage] = {ImmutableBufferUsage.MAP_READ, ImmutableBufferUsage.MAP_WRITE}, data = pointer nil) =
  if dsaEnabled(): 
    glNamedBufferStorage(GLuint buffer, GLsizeiptr size, data, usage.toBitfield(GLbitfield))
  else:
    glBindBuffer(GL_ARRAY_BUFFER, GLuint buffer)
    glBufferStorage(GL_ARRAY_BUFFER, GLsizeiptr size, data, usage.toBitfield(GLbitfield))

template setBufferData*(buffer: Buffer, data: pointer, size: int, usage: BufferUsage) =
  if dsaEnabled(): 
    glNamedBufferData(GLuint buffer, GLsizeiptr size, data, GLenum usage)
  else:
    # target doesn't matter
    glBindBuffer(GL_ARRAY_BUFFER, GLuint buffer)
    glBufferData(GL_ARRAY_BUFFER, GLsizeiptr size, data, GLenum usage)

template setBufferSubdata*(buffer: Buffer, offset: int, data: pointer, size: int) =
  if dsaEnabled(): 
    glNamedBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr size, data)
  else:
    # target doesn't matter
    glBindBuffer(GL_ARRAY_BUFFER, GLuint buffer)
    glBufferSubData(GL_ARRAY_BUFFER, GLintptr offset, GLsizeiptr size, data)

proc setBufferData*[T](buffer: TypedBuffer[T], data: openArray[T], usage = DYNAMIC_DRAW) =
  buffer.setBufferData(arrAddr data, luSizeof(T) * data.len, usage)

# note: read the note of allocDataImmutable(Buffer, int, ImmutableBufferUsage, pointer) in glw.nim
proc allocDataImmutableTyped*[T](buffer: TypedBuffer[T], data: openArray[T], usage: set[ImmutableBufferUsage] = {ImmutableBufferUsage.MAP_READ, ImmutableBufferUsage.MAP_WRITE}) =
  buffer.allocDataImmutable(luSizeof(T) * data.len, usage, arrAddr data)

proc allocDataImmutableTyped*[T](buffer: TypedBuffer[T], size: int, usage: set[ImmutableBufferUsage] = {ImmutableBufferUsage.MAP_READ, ImmutableBufferUsage.MAP_WRITE}) =
  buffer.allocDataImmutable(luSizeof(T) * size, usage)

proc getBufferSize*(buffer: Buffer): int =
  if dsaEnabled():
    if glGetNamedBufferParameteri64v != nil:
      var size: GLint64
      glGetNamedBufferParameteri64v(GLuint buffer, GL_BUFFER_SIZE, addr size)
      return int size
    else:
      var size: GLint
      glGetNamedBufferParameteriv(GLuint buffer, GL_BUFFER_SIZE, addr size)
      return int size
  else:
    glBindBuffer(GL_ARRAY_BUFFER, GLuint buffer)
    if glGetBufferParameteri64v != nil:
      var size: GLint64
      glGetBufferParameteri64v(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, addr size)
      return int size
    else:
      var size: GLint
      glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, addr size)
      return int size

proc map*(buffer: Buffer, offset = 0, size = -1, access = {BufferMapAccess.READ, BufferMapAccess.WRITE}, nonDSABindTarget: GLenum = GL_ARRAY_BUFFER): pointer =
  let realSize = if size < 0: getBufferSize(buffer) - offset else: size
  assert realSize >= 0
  if dsaEnabled(): 
    return glMapNamedBufferRange(GLuint buffer, GLintptr offset, GLsizeiptr realSize, access.toBitfield(GLbitfield))
  else:
    glBindBuffer(nonDSABindTarget, GLuint buffer)
    return glMapBufferRange(nonDSABindTarget, GLintptr offset, GLsizeiptr realSize, access.toBitfield(GLbitfield))

proc mapTyped*(buffer: TypedBuffer, offset = 0, size = -1, access = {BufferMapAccess.READ, BufferMapAccess.WRITE}, nonDSABindTarget: GLenum = GL_ARRAY_BUFFER): auto =
  return cast[ptr TypedBuffer.T](map(buffer, offset, size * luSizeof TypedBuffer.T, access, nonDSABindTarget))

proc mapTypedSpan*[T](buffer: TypedBuffer[T], offset = 0, size = -1, access = {BufferMapAccess.READ, BufferMapAccess.WRITE}, nonDSABindTarget: GLenum = GL_ARRAY_BUFFER): auto =
  var realSize = if size < 0: getBufferSize(buffer) - offset else: size
  realSize = realSize div luSizeof(T)
  return newSpan[T](mapTyped(buffer, offset, realSize, access, nonDSABindTarget), realSize)

proc unmap*(buffer: Buffer, nonDSABindTarget: GLenum = GL_ARRAY_BUFFER): bool =
  if dsaEnabled():  
    return glUnmapNamedBuffer(GLuint buffer)
  else: 
    return glUnmapBuffer(nonDSABindTarget)

macro generateTextureProcs(dimension: static[range[1..3]], targets: varargs[untyped]): untyped =
  result = newStmtList()

  for target in targets:
    let procs1 = quote do:
      proc allocTextureData*(tex: Texture[`target`], levels: int, internalFormat: TextureInternalFormat, width: int) =
        if dsaEnabled():
          if GLAD_GL_ARB_texture_storage:
            glTextureStorage1D(GLuint tex, GLsizei levels, GLenum internalFormat, GLsizei width)
          else:
            glBindTexture(GLenum `target`, GLuint tex)
            glTexImage1D(GLenum `target`, GLint levels, GLint internalFormat, GLsizei width, GLint 0, GLenum GL_RGBA, GLenum GL_UNSIGNED_BYTE, nil)
        else:
          glBindTexture(GLenum `target`, GLuint tex)
          if GLAD_GL_ARB_texture_storage:
            glTexStorage1D(GLenum `target`, GLsizei levels, GLenum internalFormat, GLsizei width)
          else:
            glTexImage1D(GLenum `target`, GLint levels, GLint internalFormat, GLsizei width, 0, GLenum GL_RGBA, GLenum GL_UNSIGNED_BYTE, nil)
      proc setTextureData*(tex: Texture[`target`], level: int, x, width: int, format: TextureFormat, datatype: TextureDataType, data: pointer) =
        if dsaEnabled():
          glTextureSubImage1D(GLuint tex, GLint level, GLint x, GLsizei width, GLenum format, GLenum datatype, data)
        else:
          glBindTexture(GLenum `target`, GLuint tex)
          glTexSubImage1D(GLenum `target`, GLint level, GLint x, GLsizei width, GLenum format, GLenum datatype, data)

    let procs2 = quote do:
      proc allocTextureData*(tex: Texture[`target`], levels: int, internalFormat: TextureInternalFormat, width, height: int) =
        if dsaEnabled():
          if GLAD_GL_ARB_texture_storage:
            glTextureStorage2D(GLuint tex, GLsizei levels, GLenum internalFormat, GLsizei width, GLsizei height)
          else:
            glBindTexture(GLenum `target`, GLuint tex)
            glTexImage2D(GLenum `target`, GLint levels, GLint internalFormat, GLsizei width, GLsizei height, 0, GLenum GL_RGBA, GLenum GL_UNSIGNED_BYTE, nil)
        else:
          glBindTexture(GLenum `target`, GLuint tex)
          if GLAD_GL_ARB_texture_storage:
            glTexStorage2D(GLenum `target`, GLsizei levels, GLenum internalFormat, GLsizei width, GLsizei height)
          else:
            glTexImage2D(GLenum `target`, GLint levels, GLint internalFormat, GLsizei width, GLsizei height, 0, GLenum GL_RGBA, GLenum GL_UNSIGNED_BYTE, nil)

      proc setTextureData*(tex: Texture[`target`], level: int, x, y, width, height: int, format: TextureFormat, datatype: TextureDataType, data: pointer) =
        if dsaEnabled():
          glTextureSubImage2D(GLuint tex, GLint level, GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum datatype, data)
        else:
          glBindTexture(GLenum `target`, GLuint tex)
          glTexSubImage2D(GLenum `target`, GLint level, GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum datatype, data)

    let procs3 = quote do:
      proc allocTextureData*(tex: Texture[`target`], levels: int, internalFormat: TextureInternalFormat, width, height, depth: int) =
        if dsaEnabled():
          if GLAD_GL_ARB_texture_storage:
            glTextureStorage3D(GLuint tex, GLsizei levels, GLenum internalFormat, GLsizei width, GLsizei height, GLsizei depth)
          else:
            glBindTexture(GLenum `target`, GLuint tex)
            glTexImage3D(GLenum `target`, GLint levels, GLint internalFormat, GLsizei width, GLsizei height, GLsizei depth, 0, GLenum GL_RGBA, GLenum GL_UNSIGNED_BYTE, nil)
        else:
          glBindTexture(GLenum `target`, GLuint tex)
          if GLAD_GL_ARB_texture_storage:
            glTexStorage3D(GLenum `target`, GLsizei levels, GLenum internalFormat, GLsizei width, GLsizei height, GLsizei depth)
          else:
            glTexImage3D(GLenum `target`, GLint levels, GLint internalFormat, GLsizei width, GLsizei height, GLsizei depth, 0, GLenum GL_RGBA, GLenum GL_UNSIGNED_BYTE, nil)
      
      proc setTextureData*(tex: Texture[`target`], level: int, x, y, z, width, height, depth: int, format: TextureFormat, datatype: TextureDataType, data: pointer) =
        if dsaEnabled():
          glTextureSubImage3D(GLuint tex, GLint level, GLint x, GLint y, GLint z, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum datatype, data)
        else:
          glBindTexture(GLenum `target`, GLuint tex)
          glTexSubImage3D(GLenum `target`, GLint level, GLint x, GLint y, GLint z, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum datatype, data)

    case dimension:
    of 1: result.add procs1
    of 2: result.add procs2
    of 3: result.add procs3

generateTextureProcs(1, TEX_1D)
generateTextureProcs(2, TEX_2D)
generateTextureProcs(3, TEX_3D)

proc enableVertexAttrib(va: VertexArray, attrib: int) {.inline.} =
  if dsaEnabled(): 
    glEnableVertexArrayAttrib(GLuint va, GLuint attrib)
  else:
    va.bindVertexArray()
    glEnableVertexAttribArray(GLuint attrib)

proc disableVertexAttrib(va: VertexArray, attrib: int) {.inline.} =
  if dsaEnabled(): 
    glDisableVertexArrayAttrib(GLuint va, GLuint attrib)
  else:
    va.bindVertexArray()
    glDisableVertexAttribArray(GLuint attrib)

proc enableVertexAttribs*(va: VertexArray, attribs: varargs[int]) {.inline.} =
  for attrib in attribs:
    enableVertexAttrib(va, attrib)

proc disableVertexAttribs*(va: VertexArray, attribs: varargs[int]) {.inline.} =
  for attrib in attribs:
    disableVertexAttrib(va, attrib)

# when not defined(AFGE_DISABLE_OPENGL_DSA):
template setVertexAttributeDSA*(va: VertexArray, attrib: int, binding: int, size: AttribSize, `type`: AttribType, offset: int) =
  if not GLAD_GL_ARB_direct_state_access:
    assert false, "disabled in non-DSA context"

  case `type`:
  of ATTRIB_DOUBLE:
    glVertexArrayAttribLFormat(GLuint va, GLuint attrib, GLint size, GLenum `type`, GLuint offset)
  of ATTRIB_FLOAT, ATTRIB_HALF_FLOAT, ATTRIB_FIXED:
    glVertexArrayAttribFormat(GLuint va, GLuint attrib, GLint size, GLenum `type`, false, GLuint offset)
  else:
    glVertexArrayAttribIFormat(GLuint va, GLuint attrib, GLint size, GLenum `type`, GLuint offset)

  glVertexArrayAttribBinding(GLuint va, GLuint attrib, GLuint binding)

template bindVertexBufferDSA*(va: VertexArray, buffer: Buffer, binding, stride, offset: int) =
  glVertexArrayVertexBuffer(GLuint va, GLuint binding, GLuint buffer, GLintptr offset, GLsizei stride)

template bindElementBufferDSA*(va: VertexArray, buffer: Buffer) =
  glVertexArrayElementBuffer(GLuint va, GLuint buffer)

template setVertexAttribute*(va: VertexArray, buffer: Buffer, attrib: int, size: AttribSize, `type`: AttribType, stride: int, offset: int) =
  if dsaEnabled():
    va.bindVertexBufferDSA(buffer, attrib, stride, 0)
    va.setVertexAttributeDSA(attrib, attrib, size, `type`, offset)
  else:
    glBindVertexArray(GLuint va)
    glBindBuffer(GL_ARRAY_BUFFER, GLuint buffer)

    case `type`:
    of ATTRIB_DOUBLE:
      glVertexAttribLPointer(GLuint attrib, GLint size, GLenum `type`, GLsizei stride, cast[system.pointer](offset))
    of ATTRIB_FLOAT, ATTRIB_HALF_FLOAT, ATTRIB_FIXED:
      glVertexAttribPointer(GLuint attrib, GLint size, GLenum `type`, false, GLsizei stride, cast[system.pointer](offset))
    else:
      glVertexAttribIPointer(GLuint attrib, GLint size, GLenum `type`, GLsizei stride, cast[system.pointer](offset))

template drawArrays*(`type`: DrawType, offset: int, count: int) =
  glDrawArrays(GLenum `type`, GLint offset, GLsizei count)

template drawElements*(`type`: DrawType, count: int, elementType: ElementType, offset: int) =
  glDrawElements(GLenum `type`, GLsizei count, GLenum elementType, cast[pointer](offset))

proc attachShader*(program: ShaderProgram, shaders: varargs[Shader]) {.inline.} =
  for shader in shaders:
    glAttachShader(GLuint program, GLuint shader)

proc detachShader*(program: ShaderProgram, shaders: varargs[Shader]) {.inline.} =
  for shader in shaders:
    glDetachShader(GLuint program, GLuint shader)

proc setShaderSource*(shader: Shader, len: int, source: cstringArray) =
  glShaderSource(GLuint shader, GLsizei len, source, nil)

proc setShaderSource*(shader: Shader, source: varargs[string]) =
  var cstrarr = allocCStringArray(source)
  glShaderSource(GLuint shader, GLsizei source.len, cstrarr, nil)
  deallocCStringArray(cstrarr)

proc compileShader*(shader: Shader): Option[string] =
  glCompileShader(GLuint shader)

  var i: GLint
  glGetShaderiv(GLuint shader, GLenum GL_COMPILE_STATUS, addr i)
  if i == GL_FALSE:
    glGetShaderiv(GLuint shader, GLenum GL_INFO_LOG_LENGTH, addr i)
    var log = cast[ptr GLchar](alloc(i * sizeof GLchar))
    glGetShaderInfoLog(GLuint shader, i, nil, log)
    result = some($log)
    dealloc(log)

proc setShaderSourceAndCompile*(shader: Shader, source: varargs[string]): Option[string] =
  setShaderSource(shader, source)
  return compileShader(shader)

proc linkProgram*(program: ShaderProgram): Option[string] =
  glLinkProgram(GLuint program)

  var i: GLint
  glGetProgramiv(GLuint program, GLenum GL_LINK_STATUS, addr i)
  if i == GL_FALSE:
    glGetProgramiv(GLuint program, GLenum GL_INFO_LOG_LENGTH, addr i)
    var log = cast[ptr GLchar](alloc(i * sizeof GLchar))
    glGetProgramInfoLog(GLuint program, i, nil, log)
    result = some($log)
    dealloc(log)

proc destroyAllShaders*(program: ShaderProgram) =
  # a sane shader program shouldn't have more than 16 shaders
  # but if you use some weird extension that must use 17+ shaders,
  # it's still supported

  var shaders: array[16, GLuint]
  var shaderCount: GLsizei
  glGetAttachedShaders(GLuint program, 16, addr shaderCount, addr shaders[0])

  if shaderCount > 16:
    var extendedShaders = newSeqUninitialized[GLuint](shaderCount)
    glGetAttachedShaders(GLuint program, shaderCount, nil, addr extendedShaders[0])

    for shader in extendedShaders:
      program.detachShader Shader shader
      destroy Shader shader
  else:
    for shader in shaders[0 ..< shaderCount]:
      program.detachShader Shader shader
      destroy Shader shader

type
  ShaderProgramStatus* = enum
    SUCCESS,
    PROGRAM_LINK_ERROR
    SHADER_COMPILATION_ERROR,

  ShaderProgramResult* = object
    program*: ShaderProgram
    status*: ShaderProgramStatus
    failedShaderType*: ShaderType
    errorMessage*: string

proc createShaderProgram*(attribs: openArray[(string, int)], sources: varargs[(ShaderType, string)]): ShaderProgramResult =
  let program = createGL(ShaderProgram)
  
  for (shaderType, source) in sources:
    let shader = createGL(Shader, shaderType)
    program.attachShader(shader)
    let compileStatus = shader.setShaderSourceAndCompile(source)
    if compileStatus.isSome:
      destroyAllShaders program
      destroy program
      result.status = SHADER_COMPILATION_ERROR
      result.failedShaderType = shaderType
      result.errorMessage = compileStatus.get
      return

  for (attribName, attribId) in attribs:
    glBindAttribLocation(GLuint program, GLuint attribId, cstring attribName)
  let linkStatus = program.linkProgram()
  program.destroyAllShaders()

  if linkStatus.isSome:
    destroy program
    result.status = PROGRAM_LINK_ERROR
    result.errorMessage = linkStatus.get
    return

  result.program = program
  result.status = SUCCESS

proc getProgram*(programResult: ShaderProgramResult): ShaderProgram =
  result = programResult.program
  case programResult.status:
    of ShaderCompilationError:
      echo "Shader Compilation Error:"
      echo "Type: ", programResult.failedShaderType
      echo "Message:"
      echo programResult.errorMessage
    of ProgramLinkError:
      echo "Message:"
      echo programResult.errorMessage
    else: discard

template use*(p: ShaderProgram) =
  glUseProgram(GLuint p)

proc glDebugCallback(source: GLenum, typ: GLenum, id: GLuint, severity: GLenum, length: GLsizei, message: ptr GLchar, userParam: pointer) {.stdcall.} =
  let sourceString = case source
    of GL_DEBUG_SOURCE_API: "API"
    of GL_DEBUG_SOURCE_APPLICATION: "APPLICATION"
    of GL_DEBUG_SOURCE_SHADER_COMPILER: "SHADER COMPILER"
    of GL_DEBUG_SOURCE_THIRD_PARTY: "THIRD PARTY"
    of GL_DEBUG_SOURCE_WINDOW_SYSTEM: "WINDOW SYSTEM"
    else: "OTHER"
  
  let typeString = case typ
    of GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: "DEPRECATED BEHAVIOR"
    of GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: "UNDEFINED BEHAVIOR"
    of GL_DEBUG_TYPE_ERROR: "ERROR"
    of GL_DEBUG_TYPE_MARKER: "MARKER"
    of GL_DEBUG_TYPE_PERFORMANCE: "PERFORMANCE"
    of GL_DEBUG_TYPE_PUSH_GROUP: "PUSH GROUP"
    of GL_DEBUG_TYPE_POP_GROUP: "POP GROUP"
    of GL_DEBUG_TYPE_PORTABILITY: "PORTABILITY"
    else: "OTHER"

  let severityString = case severity
    of GL_DEBUG_SEVERITY_NOTIFICATION: "NOTIFICATION"
    of GL_DEBUG_SEVERITY_LOW: "LOW"
    of GL_DEBUG_SEVERITY_MEDIUM: "MEDIUM"
    of GL_DEBUG_SEVERITY_HIGH: "HIGH"
    else: "OTHER"

  let message = cast[cstring](message)

  echo fmt"""OpenGL Debug Message:
  Source: {sourceString}
  Type: {typeString}
  ID: {id}
  Severity: {severityString}
  Message: {message}"""

proc enableDebug*(): bool {.discardable inline.} =
  if GLAD_GL_ARB_debug_output:
    glEnable(GL_DEBUG_OUTPUT)
    glDebugMessageCallback(glDebugCallback, nil)
    var id: GLuint = 131185
    glDebugMessageControl(GL_DEBUG_SOURCE_API, GL_DEBUG_TYPE_OTHER, GL_DONT_CARE, 1, addr id, false)

  return GLAD_GL_ARB_debug_output

template enableAlphaBlending*() =
  glEnable(GL_BLEND)
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

proc setClearColor*[C, T](color: Color[C, T]) =
  let rgbaColor = convertColor[ColorComponent.RGBA, GLfloat, C, T](color)
  glClearColor(GLfloat rgbaColor.r, GLfloat rgbaColor.g, GLfloat rgbaColor.b, GLfloat rgbaColor.a)

template clearFramebuffer*(targets: set[ClearTarget]) =
  glClear(targets.toBitfield(GLbitfield))

template enableDepthTest*() = glEnable(GL_DEPTH_TEST)

template enableCulling*(face = CullFace.BACK) =
  glEnable(GL_CULL_FACE)
  glCullFace(GLenum face)
