# assimp uses stb_image internally, so we can't use assimp as a static library (for the meantime)

import nimterop/[cimport, build]
import os

const 
  ASSIMP_GIT_URL {.strdefine.} = "https://github.com/assimp/assimp"
  baseDir = getProjectCacheDir("afge" / "assimp")

setDefines(@["cimportGit"])

proc cimportPreBuild(outdir, header: string) =
  let apiFile = outdir / "include" / "assimp" / "assimp.h"
  apiFile.writeFile("""
#include "cimport.h"
#include "scene.h"
#include "postprocess.h"
#include "defs.h"
""")

getHeader(
  "cimport.h",
  giturl = ASSIMP_GIT_URL,
  outdir = baseDir,
  cmakeFlags = "-DBUILD_SHARED_LIBS=ON",
  altNames = "assimp"
)

cIncludeDir(baseDir / "include")
cIncludeDir(baseDir / "buildcache" / "include")

cPlugin:
  proc onSymbol*(sym: var Symbol) {.exportc, dynlib.} =
    while sym.name[0] == '_':
      sym.name = sym.name[1..^1]
cOverride:
  const
    aiComponent_Force32Bit* = 0i32.aiComponent
    aiProcess_GenBoundingBoxes* = aiPostProcessSteps(cint(-0x80000000))
    ai_epsilon = cfloat(0.00001)
  type
    aiSceneFlags* {.pure.} = enum
      INCOMPLETE = 0x00000001
      VALIDATED = 0x00000002
      VALIDATION_WARNING = 0x00000004
      NON_VERBOSE_FORMAT = 0x00000008
      TERRAIN = 0x00000010
      ALLOW_SHARED = 0x00000020
    
when defined(unix):
  cPassL("-lm")
  cPassL("-lpthread")
  cPassL("-lstdc++")
cImport(baseDir / "include" / "assimp" / "assimp.h", recurse = true, dynlib = "cimportLPath")
