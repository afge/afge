
import typetraits
import dynamic_struct
import macros

template isDynamicType*[T](t: typedesc[T]): bool =
  when compiles(distinctBase(t)):
    distinctBase(t) is DynamicObject
  else:
    false

proc align*(offset, align: int): int =
  return ((offset - 1) or (align - 1)) + 1

# procs with prefix lu (layoututils) works with tuple, objects and dynamic objects
# using that is highly advisable instead of nim's built-in procs
template luSizeof*[T](t: typedesc[T]): int =
  when isDynamicType(t):
    dynSizeof(t)[]
  else:
    sizeof(T)

template luSizeof*[T](x: T): int = luSizeof typeof x

template luSizeofField*[T](t: typedesc[T], field: untyped): int =
  when isDynamicType(t):
    luSizeof((ptr T)(nil).field()[])
  else:
    luSizeof((T).field)

template luSizeofField*[T](x: T): int = luSizeofField typeof x

template luOffsetof*[T](t: typedesc[T], field: untyped): int =
  when isDynamicType(t):
    offset(t, field)
  else:
    offsetof(t, field)

template luOffsetof*[T](t: T, field: untyped): int =
  luOffsetof(typeof t, field)

template luFieldPairs*[T](x: T, callback: untyped) =
  when compiles(x[]):
    when isDynamicType(typeof x[]):
      iterateFields(x, callback)
    else:
      for fieldName, fieldValue in fieldPairs(x):
        callback(fieldName, fieldValue)
  else: 
    for fieldName, fieldValue in fieldPairs(x):
      callback(fieldName, fieldValue)

template calculateStructLayout*[T](t: typedesc[T]) =
  static:
    assert distinctBase(t) is DynamicObject
  var curOffset = 0
  var obj = (ptr T) nil
  template callback2(fieldName: static[string], fieldValue: typed) =
    offsetStr(t, fieldName) = ((curOffset - 1) or (alignStr(t, fieldName) - 1)) + 1
    curOffset = offsetStr(t, fieldName) + luSizeof(typeof fieldValue)
  luFieldPairs(obj, callback2) # luFieldPairs internally use a callback, so the name must be callback2
  calcSizeAlignof(t)

template calculateUnionLayout*[T](t: typedesc[T]) =
  static:
    assert distinctBase(t) is DynamicObject
  var obj = (ptr T) nil
  template callback2(fieldName: static[string], fieldValue: typed) =
    offsetStr(t, fieldName) = 0
  luFieldPairs(obj, callback2) # luFieldPairs internally use a callback, so the name must be callback2
  calcSizeAlignof(t)

when isMainModule:
  type Y = tuple[x: int, y: string]

  dynamic:
    type
      X* = object
        x: int
        y: int

  echo luSizeof(string)
  echo luSizeof Y
  echo luSizeof X

  X.offset(x) = 10
  X.calcSizeAlignof()

  echo luSizeof X

  X.offset(x) = 0
  X.align(x) = 16
  X.calcSizeAlignof()
  echo luSizeof X

  let dyn = allocX()
  dyn.x = 10

  let sta = (x: 10, y: "hello")

  template print(name: static[string], value: typed) =
    echo typeof value

  luFieldPairs(dyn, print)
  luFieldPairs(sta, print)

  echo X.align(x)
