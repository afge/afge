#version 460

#ifndef AFGE_R2D_FRAGMENT
#define AFGE_R2D_FRAGMENT

#ifndef GLSLLINT
#include "../layout.glslib"
#include "common.glslib"
#else
#define AFGE_R2D_PAINT_DATA_VEC4S 1
#define uint64_t int
uniform UniformBlock { mat4 ortho; vec4 paintData[1]; uint64_t textures[1]; } Uniform;
uniform sampler2D tex0, tex1, tex2, tex3;
#endif

in vec2 fPos;
in vec2 fTexCoords;
in vec4 fColor;
#ifndef GLSLLINT
flat in int fPaintId;
#else
in int fPaintId;
#endif

out vec4 oColor;

vec4 AFGE_R2D_GetPaintData(int vectorIndex) {
    return Uniform.paintData[fPaintId * AFGE_R2D_PAINT_DATA_VEC4S + vectorIndex];
}

vec4 AFGE_R2D_SampleTexture(int texId, vec2 texCoords) {
#ifdef AFGE_R2D_BINDLESS_TEXTURE
    return texture(sampler2D(Uniform.textures[texId]), fTexCoords);
#else
#define AFGE_R2D_CSSMPLTEX(i, t) case i: return texture(t, fTexCoords)
    switch(int(texId)) {
#if AFGE_R2D_MAX_TEXTURES > 0
        AFGE_R2D_CSSMPLTEX(0, tex0);
#endif
#if AFGE_R2D_MAX_TEXTURES > 1
        AFGE_R2D_CSSMPLTEX(1, tex1);
#endif
#if AFGE_R2D_MAX_TEXTURES > 2
        AFGE_R2D_CSSMPLTEX(2, tex2);
#endif
#if AFGE_R2D_MAX_TEXTURES > 3
        AFGE_R2D_CSSMPLTEX(3, tex3);
#endif
    }
    return vec4(0.0);
#endif
}

#ifndef AFGE_R2D_NO_STANDARD_PAINTS
#define AFGE_R2D_SIMPLE_PAINT 0
#define AFGE_R2D_TEXTURE_PAINT 1
#define AFGE_R2D_MAX_PAINT_TYPES 32

#define AFGE_R2D_PAINT_TYPE_0
void AFGE_R2D_Paint_0() {
    oColor = fColor;
}
#define AFGE_R2D_PAINT_TYPE_1
void AFGE_R2D_Paint_1() {
    vec4 data = AFGE_R2D_GetPaintData(0);
    oColor = AFGE_R2D_SampleTexture(int(data.y), fTexCoords);
}
#endif

void AFGE_R2D_Paint() {
    vec4 data = AFGE_R2D_GetPaintData(0);
    int paintType = int(data.x);
    if(paintType < 0 || paintType >= AFGE_R2D_MAX_PAINT_TYPES) {
#ifndef GLSLLINT
        discard;
#endif
    }

    switch (paintType) {
        case 0:
#ifdef AFGE_R2D_PAINT_TYPE_0
            AFGE_R2D_Paint_0();
#endif
            break;
        case 1:
#ifdef AFGE_R2D_PAINT_TYPE_1
            AFGE_R2D_Paint_1();
#endif
            break;
        case 2:
#ifdef AFGE_R2D_PAINT_TYPE_2
            AFGE_R2D_Paint_2();
#endif
            break;
        case 3:
#ifdef AFGE_R2D_PAINT_TYPE_3
            AFGE_R2D_Paint_3();
#endif
            break;
    }
}

void main() {
#ifdef AFGE_R2D_BEFORE_MAIN
    BeforeMain();
#endif
    AFGE_R2D_Paint();
#ifdef AFGE_R2D_AFTER_MAIN
    AfterMain();
#endif
}

#endif